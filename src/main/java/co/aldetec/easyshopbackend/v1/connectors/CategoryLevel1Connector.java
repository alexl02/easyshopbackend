package co.aldetec.easyshopbackend.v1.connectors;

import co.aldetec.easyshopbackend.v1.persistence.dao.BranchDAO;
import co.aldetec.easyshopbackend.v1.persistence.dao.CategoryLevel1DAO;
import co.aldetec.easyshopbackend.v1.persistence.dao.CompanyDAO;
import co.aldetec.easyshopbackend.v1.persistence.entities.CategoryLevel1;
import co.aldetec.easyshopbackend.v1.persistence.entities.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class CategoryLevel1Connector {

    @Autowired
    private CompanyDAO companyDAO;

    @Autowired
    private CategoryLevel1DAO categoryLevel1DAO;

    @Autowired
    private Environment env;

    public CategoryLevel1Connector() {
    }

    public void loadConnector(Company company) throws Exception {
        String connectorsPath = env.getProperty("easyshop.connectors.path");
        String line = new String();
        File file = null;
        FileReader fr = null;
        BufferedReader br = null;
        try{
            file = new File (connectorsPath + "/category_l1_connector.csv");
            fr = new FileReader (file);
            br = new BufferedReader(fr);

            List<CategoryLevel1> categoryLevel1List = new ArrayList<CategoryLevel1>();
            int lineNum = 0;
            while( (line=br.readLine()) != null) {
                if(lineNum < 1){
                    lineNum++;
                    continue;
                }
                CategoryLevel1 categoryLevel1 = new CategoryLevel1();
                String[] fields = line.split(";");

                categoryLevel1.setCompany(company);
                categoryLevel1.setAlterCode(fields[0]);
                categoryLevel1.setName(fields[1]);
                categoryLevel1.setImgUrl(fields[2]);
                categoryLevel1.setSearchableWords(fields[3]);
                categoryLevel1.setState(true);
                categoryLevel1List.add(categoryLevel1);
                System.out.println(line);
            }
            categoryLevel1DAO.updateMany(categoryLevel1List);
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel1Connector.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            throw new Exception(ex);
        } finally {
            try{
                if( null != fr ){
                    fr.close();
                }
            }catch (Exception ex){
                Logger.getLogger(CategoryLevel1Connector.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
                throw new Exception(ex);
            }
        }

    }

}
