package co.aldetec.easyshopbackend.v1.connectors;

import co.aldetec.easyshopbackend.v1.persistence.dao.CategoryLevel1DAO;
import co.aldetec.easyshopbackend.v1.persistence.dao.CategoryLevel2DAO;
import co.aldetec.easyshopbackend.v1.persistence.dao.CompanyDAO;
import co.aldetec.easyshopbackend.v1.persistence.entities.CategoryLevel1;
import co.aldetec.easyshopbackend.v1.persistence.entities.CategoryLevel2;
import co.aldetec.easyshopbackend.v1.persistence.entities.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class CategoryLevel2Connector {
    @Autowired
    private CompanyDAO companyDAO;

    @Autowired
    private CategoryLevel1DAO categoryLevel1DAO;

    @Autowired
    private CategoryLevel2DAO categoryLevel2DAO;

    @Autowired
    private Environment env;

    public void loadConnector(Company company) throws Exception {
        String connectorsPath = env.getProperty("easyshop.connectors.path");
        String line = new String();
        File file = null;
        FileReader fr = null;
        BufferedReader br = null;
        try{
            file = new File (connectorsPath + "/category_l2_connector.csv");
            fr = new FileReader (file);
            br = new BufferedReader(fr);

            List<CategoryLevel2> categoryLevel2List = new ArrayList<CategoryLevel2>();
            int lineNum = 0;
            while( (line=br.readLine()) != null) {
                if(lineNum < 1){
                    lineNum++;
                    continue;
                }
                CategoryLevel2 categoryLevel2 = new CategoryLevel2();
                String[] fields = line.split(";");

                CategoryLevel1 categoryLevel1 = categoryLevel1DAO.findByAlterCode(company, fields[0]);
                if(categoryLevel1 == null){
                    continue;
                }

                categoryLevel2.setCategoryLevel1(categoryLevel1);
                categoryLevel2.setAlterCode(fields[1]);
                categoryLevel2.setName(fields[2]);
                categoryLevel2.setSearchableWords(fields[3]);
                categoryLevel2.setState(true);
                categoryLevel2List.add(categoryLevel2);
                System.out.println(line);
            }
            categoryLevel2DAO.updateMany(categoryLevel2List);
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel1Connector.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            throw new Exception(ex);
        } finally {
            try{
                if( null != fr ){
                    fr.close();
                }
            }catch (Exception ex){
                Logger.getLogger(CategoryLevel1Connector.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
                throw new Exception(ex);
            }
        }

    }
}
