package co.aldetec.easyshopbackend.v1.connectors;

import co.aldetec.easyshopbackend.v1.persistence.dao.BranchDAO;
import co.aldetec.easyshopbackend.v1.persistence.dao.CategoryLevel2DAO;
import co.aldetec.easyshopbackend.v1.persistence.dao.ErpProductDAO;
import co.aldetec.easyshopbackend.v1.persistence.dao.ProductDAO;
import co.aldetec.easyshopbackend.v1.persistence.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class ProductConnector {
    @Autowired
    private CategoryLevel2DAO categoryLevel2DAO;

    @Autowired
    private Environment env;

    @Autowired
    private BranchDAO branchDAO;

    @Autowired
    private ErpProductDAO erpProductDAO;

    @Autowired
    private ProductDAO productDAO;

    public void loadConnector(Company company) throws Exception {
        String connectorsPath = env.getProperty("easyshop.connectors.path");
        String line = new String();
        File file = null;
        FileReader fr = null;
        BufferedReader br = null;
        try{
            file = new File (connectorsPath + "/product_connector.csv");
            fr = new FileReader (file);
            br = new BufferedReader(fr);
            List<String> data = new ArrayList<String>();

            int lineNum = 0;
            while( (line=br.readLine()) != null) {
                if(lineNum < 1){
                    lineNum++;
                    continue;
                }
                data.add(line);
                System.out.println(line);
            }
            productDAO.updateMany(company, data);
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel1Connector.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            throw new Exception(ex);
        } finally {
            try{
                if( null != fr ){
                    fr.close();
                }
            }catch (Exception ex){
                Logger.getLogger(CategoryLevel1Connector.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
                throw new Exception(ex);
            }
        }

    }
}
