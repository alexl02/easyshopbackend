package co.aldetec.easyshopbackend.v1.mail;

import co.aldetec.easyshopbackend.v1.persistence.entities.Order;
import co.aldetec.easyshopbackend.v1.persistence.entities.OrderDetail;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


@Service
public class Mail {

    public void sendCustomerMail(Order order) {
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###.###");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
        Properties props = new Properties();
        props.setProperty("mail.smtp.host", "mail.eljardin.co"); // Nombre del host de correo, es smtp.gmail.com
        props.setProperty("mail.smtp.ssl.trust", "mail.eljardin.co");
        props.setProperty("mail.smtp.starttls.enable", "true"); // TLS si está disponible
        //props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.smtp.port", "25"); //Puerto del servidor para envio de correos
        props.setProperty("mail.smtp.user", "servicioalcliente@listao.co"); // Nombre del usuario
        props.setProperty("mail.smtp.auth", "true"); // Si requiere o no usuario y password para conectarse.
        Session session = Session.getDefaultInstance(props);
        session.setDebug(true);

        Double total = 0.0;
        String msgDetail = "";
        Boolean alternRowInd = false;
        for(OrderDetail orderDetail : order.getOrderDetails()){
            total = total + (orderDetail.getQty() * orderDetail.getDiscountPrice());
            String rowClass = "";
            if(alternRowInd){
                rowClass = "order-detail-table-row-b";
            }
            msgDetail = msgDetail +
                    "                        <tr class=\"" + rowClass + "\">\n" +
                    "                            <td>" + orderDetail.getProduct().getErpProduct().getSku() + "</td>\n" +
                    "                            <td>" + orderDetail.getProduct().getErpProduct().getEan() + "</td>\n" +
                    "                            <td>" + orderDetail.getProduct().getName() + "</td>\n" +
                    "                            <td>" + orderDetail.getMu() + "</td>\n" +
                    "                            <td>$" + decimalFormat.format(orderDetail.getDiscountPrice()) + "</td>\n" +
                    "                            <td>" + decimalFormat.format(orderDetail.getQty()) + "</td>\n" +
                    "                            <td>$" + decimalFormat.format(orderDetail.getDiscountPrice() * orderDetail.getQty()) + "</td>\n" +
                    "                        </tr>\n";
            alternRowInd = !alternRowInd;
        }

        String msg = "<!DOCTYPE html>\n" +
                "<html lang=\"es\">\n" +
                "  <head>\n" +
                "    <meta charset=\"utf-8\" />\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n" +
                "    <style>\n" +
                "        body {\n" +
                "            margin: 0;\n" +
                "            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',\n" +
                "                'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',\n" +
                "                sans-serif;\n" +
                "            -webkit-font-smoothing: antialiased;\n" +
                "            -moz-osx-font-smoothing: grayscale;\n" +
                "            background-color: white;\n" +
                "            color: black;\n" +
                "        }\n" +
                "\n" +
                "        .header {\n" +
                "            display: flex;\n" +
                "            justify-content: center;\n" +
                "            align-items: center;\n" +
                "            width: 100%;\n" +
                "            height: 100px;\n" +
                "            background-color: #00b9e2;\n" +
                "        }\n" +
                "\n" +
                "        .header-logo-container {\n" +
                "            \n" +
                "        }\n" +
                "\n" +
                "        .main-container {\n" +
                "            padding: 50px;\n" +
                "        }\n" +
                "\n" +
                "        .customer-info {\n" +
                "            display: flex;\n" +
                "            flex-direction: column;\n" +
                "            align-items: center;\n" +
                "            width: 100%;\n" +
                "            margin-top: 50px;\n" +
                "            font-size: 25px;\n" +
                "            gap: 20px;\n" +
                "        }\n" +
                "\n" +
                "        .customer-info-name {\n" +
                "            font-weight: bold;\n" +
                "        }\n" +
                "\n" +
                "        .customer-info-thanks {\n" +
                "            font-weight: bold;\n" +
                "            color: #00b9e2;\n" +
                "        }\n" +
                "\n" +
                "        .order-info {\n" +
                "            margin-top: 50px;\n" +
                "        }\n" +
                "\n" +
                "        .order-header {\n" +
                "            display: flex;\n" +
                "            flex-direction: column;\n" +
                "            font-size: 18px;\n" +
                "            gap: 10px;\n" +
                "        }\n" +
                "\n" +
                "        .order-field {\n" +
                "            display: flex;\n" +
                "        }\n" +
                "\n" +
                "        .order-field-title {\n" +
                "            font-weight: bold;\n" +
                "            width: 300px;\n" +
                "        }\n" +
                "\n" +
                "        .order-field-title2{\n" +
                "            font-weight: bold;\n" +
                "            font-size: 30px;\n" +
                "            width: 300px;\n" +
                "        }\n" +
                "\n" +
                "        .separator {\n" +
                "            width: 100%;\n" +
                "            border-bottom: solid 1px #565656;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-container {\n" +
                "            margin-top: 50px;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-title-container {\n" +
                "            display: flex;\n" +
                "            flex-direction: column;\n" +
                "            align-items: center;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-title {\n" +
                "            font-weight: bold;\n" +
                "            font-size: 20px;\n" +
                "            margin-top: 20px;\n" +
                "            margin-bottom: 20px;\n" +
                "        }\n" +
                "        \n" +
                "        .order-detail-table{\n" +
                "            border-top-left-radius: 5px;\n" +
                "            border-top-right-radius: 5px;\n" +
                "            margin-top: 20px;\n" +
                "            width: 100%;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-table th{\n" +
                "            background-color: #00b9e2;\n" +
                "            color: white;\n" +
                "            height: 30px;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-table td{\n" +
                "            text-align: center;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-table td{\n" +
                "            height: 30px;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-table-f-th{\n" +
                "            border-top-left-radius: 5px;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-table-l-th{\n" +
                "            border-top-right-radius: 5px;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-table-row-b {\n" +
                "            background-color: #00b9e222\n" +
                "        }\n" +
                "        \n" +
                "        .order-detail-footer-container {\n" +
                "            display: flex;\n" +
                "            flex-direction: column;\n" +
                "            margin-top: 20px;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-footer-total-container {\n" +
                "            display: flex;\n" +
                "            justify-content: right;\n" +
                "            font-size: 20px;\n" +
                "            margin-top: 20px;\n" +
                "            margin-bottom: 20px;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-footer-total-title {\n" +
                "            font-weight: bold;\n" +
                "            width: 100px;\n" +
                "        }\n" +
                "\n" +
                "        .footer {\n" +
                "            display: flex;\n" +
                "            justify-content: center;\n" +
                "            align-items: center;\n" +
                "            width: 100%;\n" +
                "            height: 70px;\n" +
                "            background-color: #00b9e2;\n" +
                "        }\n" +
                "\n" +
                "        .footer-link {\n" +
                "            color: white;\n" +
                "            font-weight: bold;\n" +
                "            font-size: 30px;\n" +
                "            text-decoration: none;\n" +
                "        }\n" +
                "    </style>\n" +
                "    <title>Listao</title>\n" +
                "  </head>\n" +
                "  <body>\n" +
                "    <div class=\"header\">\n" +
                "        <div class=\"header-logo-container\">\n" +
                "            <img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAAA+CAYAAAAClQafAAAAAXNSR0IArs4c6QAAErdJREFUeAHtnQuYVdV1x/edJ8NDZgbUQYaAJRBQ1NRHHgQCApr6iDWaF0nTElEr2qix5muMTYk2SY1fqWmMiCZolGisGkRapP0ERxEDUtSKTREQ1OCAojxUBgbmcfv7D3MnZ+6ce2fvc+6duffOWd/3n3PO3mutvfa6e529z977nImZZFpZP5GkWca0nslxuImbimSWPLo+bGKxncbEnzMx84CZNuKJPLI9MjXyQMY8EOvQtH7PYLPvwEKC4uKOtEI7iZlnjamYaaYPrS+0qkX1iTyQzgNHAr1ub6Vp3b+a3vvEdMwFkRczb5uS0olmSs3rBVGfqBKRByw8UNTG09pwf58IclU2bmpMc/MSUxcvsfBPxBJ5oCA8UGTq6meYePzzBVEb20rE4yeblvrZtuwRX+SBfPdAkWlpnZXvlQhof1+td0B3RWL57IEiZqUn5XMFAtseM5806+OlgeUjwcgDeeQBnlPjx+WRvZkzNR6PmYYdNSjcHkZpPB7/JfITAujYGIvFvmkrRznD4P1bMAOMAIfAJvAouAtdzRwjijzg64ESJqf6bq/WXFLm6xW3xMmwj3UTaeM+2laGIL8M3ltBZZKMgn8quAyeSwn29Un50WXkgTYPHJl1j5yRsx4ggK/BuLtBcpB7bT6Fi2fgHedNjM4jDyQ8EAV6whM5eCRwT8Ksn1ia1h++B5HJxCjFssiILV88EAV6bv9SN2FeuYOJfwrvlx34I9Y+4oEo0HP7h9bEmyud5SoQ8Re+B6LdYTn6GzMEH4BpgwKYp5WEvKf2+v8ZFSlnkvHBvK9QL1cg6tHD/wAt4VV01UDjbiBVS2iutMdVIFf4Ce6jwWzw79j0HtDS4ddyxb58tiPq0cP/ehoqV4GPg0Xh1XXSsJqr6Z1Sur94tnuW3OEgqEdjzYXt0CvSUeeThZ8nCvSQTqXnrUdFPQ1Ws96ZpltQOA388XXi9CX8gewH0rP0fi6+0nLgF4ECfELvW1T4FoQKdN16x/QvMYOKbduhnUP3NbearQdb2MvjT/0oeGS/EvP6wWZzOBWTv2hepXITWUFQ3IbR11kY3grPN5B534K3t1k0UhnY20b0pfIDB/oZg0rNwycNMaMqAqtI6+eNDU3m4g27zcYDnXd2Xnpcf/OvYytN/+Iis6+p1czeuNcsfvdgWl15nnkD9uueenX70a86ep69iiBf5ZeZg2nFOWhTQZukBuRM6r+zGeQyaPyAUrPoxOpOtp0woMQsGFfVFuTKqCwtgqfK1JQFqkYn3bl6QfAeBt/Gvk+B5aDRY+suzueDcfA87EmPTiMPdPJAoO74I/2Ks9aTe6077agyM6CId09aj4zPJw4uM8Wxzo8J6tlPG1Rmlu32tn+vlsI4J5D/m5qcy1BevaH2yTeStq8wateztcCH6hkqwVFAqyb7BfzZxLEgKVCg7zjUYvbzHD2wJLs9aX1jS0eQy/ubk4bxSuNHM1t80pVXiERjVMN8O1t1w5/6GOghytEzf0EQddLQ8GxwDjgV6CWkLluF4dtBut4IfBU8A57ED72yXIktsm8o0BLrHuwINRsVKNCbKHLOpn1m4fgqU0aPmw060NJqrnh1byfVq/YdNgvrG8zs4dpLcoR+/MaHZjOTcoVE/MgzqE9tgDq9RYNYYStHOaXwalPKBeATQAHQDzSRp9WEN8F/gd+g9w2OHUS+bgjng1QN4H+Q2SwBeI/hcK7OIfGrXFsaifwsH+Yt6H/OJ70tCRlN9v0VmAn02GMzL3AcfMKZYA5oQc8ajreD31JeVvZMUIZGaNPAVDAZjAAabSSoGZ53udgOVoM68Cz2WE+8xsyK7YHvFKMris2UynIzKMM9uybZntp7yGxn5OBHn2ZIf8LAEvPiB03mpf0hRluxso+a6cdu9SvDNY0fQoHyvIPcNn6o0X786FKAjPHL6yZtKzo/2g2PAk/7568ENwCb12XVRhTwmvDbxlE6FOTa2JKKfgnvZcqE95sc7knFGDB9NfoVFJ2Iso4n4WpwCfAGSye+ABdqJzdS5r8FkPUVwdbTydCKypeAa6er3u0RMA+bXuCYllyVd1KmJbCtBw90SuuJizUfHDZCRO4eoHHpBrIEnOAgrV5YPf8ryF9Lw/oF5yMd5LPOil3DKOQHQAEeql0j70e6KT9EOZ/j+C180ODHZJOGDvlOPjzLhj8Fj+qo0cpM9D3F8Qps2pKCN9qFlMoxhZhOg9Brr+uAS5B7XdGfi7vQ8zWO6jlziZZizOUgG0HuradGJ6vwgXzhTMhJfgMIE+TJ5WrY/xK6dZPzpSLf1Cix4DxAIxhMpR4Dmm0OQ+rd7wXqTXKJtPTYU6QJvfvwqXxhTfDPg1mPMJl8pEiUr4mrhZRxRyLBewx996vgVjGAJa6epkMsuX3YokfHiCw98C34fOcELOW9bJoR1qRVLtH9GPP9HjRIW3ivAj+3KZMA/BF8eh7PNl1JWe8xjJ/rLShwoGsi7p7x1WZSZZkpSlrb9haQzfPfMxF3OTPzv3s/el5P52d+eN2Jr0zHk+95NOzXqOdq6jGpB+tyHWXOp+zWdGXCcwX530vHk+G8f6DMN7BLI682CtwV/5btr5+tKu+1IJf1Jw4sNUtPGWKqSpxGUEdq3rf+1lJdTVbZkpYy1Ft9BIwDfw60xNTzM68U6kC/SsPbQt6LQBORC8BtQIGwDFgvU8HrpeO50OpDSiLg5PefpGRIn6EJP82uB6F/puzqhGCgHr22vNicwm60XKAhpcXmU4PLzfLC2hn3G3w7FtSAqSAsqUG6kD4fPd8jsInzpTScmzhqqPpVT57N6cswPdTOqLvyl4BtJ/MevCvaZb2Hdd6L9nMtN/0MJCbKDnG+GKjsZ6iTb0BTL8XBhWAhcH1+1hB+KUhFPyXDVedeZGYBLV8q0HSuG1MFsCUF+Q/A1RIIFOi7m1rMYZ6Rs7VZRoa5kHbqFRLRINuer2iAU6mXEJYUXC60y48Zu3Zj09fJ2w8u9ePxS0PuRdJnJvLQcQHnto32FeQ7ZBM6/I7wfYDuxeSdCxQYd8pmP15vGjzqNR9FdgfH57x5Fucpb6LoG4/8ly10eFn0GPB5bErYoZuVVjq2c9Tow4XmIHcLunbY3lU7KT+IKd/f5ntz7MTXExeLdjaYl8NsmukJI3u/jLcdTbieBqIergvRaNQQNbH3TpfM3EiYgxmjsPOHwDfIqZtvu4f/d8gq2F1oVBpm15GPVD2AHYkg71BN2hNc6CbmQomRSrAeXSXd+uZ+s5ZJsBnV/cxRGX4f3aYmjYwo1rJpZsm7jTbsfZ3nNRyg4WCVpSOOgu8xAuIljveBR2hoHQHAeSN5GpKeB1LRC6kyspmObRptdBB2ap5BPfxkcCo4GlSRrnmInWAD0BD5oXbZfZy7rCgchy7etfLdHvsVdLnS/WkEFpF3UZp8v6wvkDg/1BZYP615lZajW2ATPqQBTeW8LnFtcUy5BRZdakDfsNDhx6JeXL2depTHaNRvcAxM2KJJPduhex3lTXMtjDI+jcx3wAWg2EJevb9mxq8HYyz4vSz6gGWnpR/K15B+m5fJ4lw3nlp0yd9dCJ3lJOqxSjdiW9JjSaXvEMZWQ8SXVx74J6xtCWix2skk8C/gdRrcOnANODagvqyJYVN/8DMK0PBXvZlNkMueIeAu4BrkkvUjjSRcaUOqIJci8vS8/oqjUg3fR0aB7ui1fGWnkWzE9h9myP4z0KOhu76V9wT4TIb0hlKDHZUoWAU0h+A6ARmqbB/hWp+07pLqu2MgX72+K9VGge7qsvzmvwnzvctmYWuj3vIcsJog0zP9x8IqDCpP2QOQfRKcFlRHhuWyFeiuE6uqVq269UA0lM84zRsz2JzFZFymPw5pY9AhPjihycDrNr9fcO+j29Q/CA+9ehy5qwiKZzjeDQYH0ZNC5kLSz0f3dylnXgqebCbfjPLTs1mAo27f52xHHX7sQWaf+QhbQHrkpGoztapfQOnwYvqqwHlDK8wEvi134tp3On2JJrz2wtZAID5MQK6jlhrKa523NEM1VnvSjixNTv04Qzq7VUN5J8F0TbeMPcsQpOcdbmFijQVPMsuuQEN3fYyxN4PcW4uRfIV2IvvtI3LzAIGovdB/gdRIoN5wu5uGtNw3E3ya9e4pupSCbCfdEjZt5eRecAt4AGR6X0CQQLdZ1uu5QG/grbFWhs65Qh82544tueITWzsI9p1gLtAssXrGvwN1QDO8QUlBNzeNsMsPlpaXG4o6K41KXOhOmMdT50vADUA3vFHgdpApCjJpZjPjPzqAge8EGrrr9dCfv7XfXD1iUIAyMyuyck+jeT762kxapxIM02BI9/KFPj6onWT/C59wKzJ6LpsIpgMtU40HLjQDHcei06+n1E2kv6Wy5m74hpHv0stp7/y3sUsbZjqIa20C0vD/YmDTs3bIpjj5P9LVq7vYNhobTseW9X46yZtA+vF+eWnSVN9NgQJdSq9lEuwFvtl2VnXmvxmXxuiOLL2Prsm4+dxw0t7yOyT69Ik2jaR9hqURPU4D61ijVcNH5ql23Ei+dpf9GtjurlOvrobpF+gHHPR093jpOrv9e+rmO1ohnWrGffOw14nQxaA3/hhCc5wEj3zQ0jfQ0fNFR11iX4YtLYEDXcF1/9sH2hCg8Egk9zywgIaplyn2+JlGutbL1XAv8ctPkZYqCBtS8PslD/FL9KS57BKTWCqb9BFLTfZomS5T9CiKXAN9Fnbchr+3eY0gTXsE/tqbZnm+VHyBA92ykIJn4wfQ7LKGeq67xGqQ/ZXHQdpa+riuSdd690hgMwsrkQQN8+jUdth/TGRYHDVM34j89Ry1t109egeRrqG2eFwo1U68d1Ey1lLRyZQ9Bnu2JPi51n71E0jTMuGORLrlUcNjfUhxgZefNN1Q7gPHeNMtz/WWWGIk8Ca6l7fLPc3xRXBq+7XNYSBMy9B3Pno0Yaj2MJiDbhoujwESlfx/6CTa6x7yc8/8CLvxY7WcGZLu4If9G+lA51scXIM8uXjNqrc9z6FPu9jSDt2ThA9y/TR4Cah+CoKLwDjgQpOxYXWyAPYsJk3P/bb0KoyaGVdvq5vNZ8AadE9GlyaKPgCutAwBPZocBh8HmtDLxKSTtrGegq42wr5PcLIGdPcIckTgj3+bOJV9Gv1MBUHa2BewZQmyUY8uJ0TUxQMVpJzTji6ZlgkKPvVmfrTZLzFNmm4wvqMTGvKHBNMq8j+bRt4v6zwShawS9um9gF9QiOuwW3sbPhfCuJWJIJcO17tMiHIj0T7mAb1XrUk3P3rBLzFE2j0hZHtC9FoKWdETBbWXsYnjV73lRYHu9UZ0nikPvIOiuWmU6dlaQ+ZM0YMoWp8pZZnWww2vEZ0XgKczrdtHnx77zqbM97x5UaB7vdF3zzVp0zY/kAEXqBefSUPThJsvkbeLDD2nZ4TQp+fZr4Mgz+p+NqQaifjxWqVho+Y9zgd3WwkEY9J8yCTK+kOyeBToyR7pm9daW76Dql8JwvS06smnoavOwo03wrPHgs+KhTL13D8Z1FsJpGZ6mSztUNPGoYwSNjYAPaufDboEY4jCdKP7ezAF/W/66YkC3c8rfS9Nz3T6sMGdHE4Gmu11Id0cbgcfQ8fzNoLwbYNPG0A6DTFtZFPxoHMDeaeDRUBbPVxpJQJT0bOD43ddhW350f8kvGPBLBBmvmIv8reAUej8EWjl3JdKfFP7SmJZoMaQ7J3vkHBscmKAa28v+D3khwfQ4RXx9hhan2/2ZiadP5i4prEo6KczU6wlJ03oXAT+BBQDL73PxTqgZapfI7fbm2lzjkwd5YyHVzPqF4OjLeQ0rNZylda8uxA6te30L9H7U46Xga+AKpCO1pJ5E7L/mWDiXGvZV3BdnUizPL5mw4d+rburDvrXTro5zQAakUwElcCPFMivAM1xrALL0WP1mKF1dP1ArpVBpACofFC1mVypu2JEaTxAQ1SQ14BhoBFoyK2XYYL0moh2JcrQ6PI0oMAf0o5yjrqh7ASaZNLwXPsDrMtFr5apJgAF0yigoFcHp3mCV4GWoaQ/Jwh7YxiiQJcPhoIyIH8rTndjq0ZPzhQzK99ayw6NTzpL5ruAJoum1x6T79WI7I88YOMB7qLxJTaMhccTf7zw6hTVKPKAvweKTNHABUzDOD9f+avLk9SYaWLwdmueWBuZGXkgtAeKzJlV+0xR/HKmXK2fe0KX2tsK4rEbzJQRW3rbjKj8yAM95QFNgLDyOWIxvfpszgI96LfpyIc/bTezorlmRu28fDA3sjHyQKY8cCTQpW368Hv5ewY9+/LC7N1jaxi5TDMzht+cKedFeiIP5IsHNJXflep2sZTSNIXZ+OGsNFd0ZciTFC1FtLbuNKWlz5kpNa/nidWRmZEHMu6B/wfdmyQLx31Y2wAAAABJRU5ErkJggg==\" alt=\"logo listao\">\n" +
                "        </div>\n" +
                "    </div>\n" +
                "    <div class=\"main-container\">\n" +
                "        <div class=\"customer-info\">\n" +
                "            <div class=\"customer-info-name\">" + order.getCustomer().getName() + "</div>\n" +
                "            <div class=\"customer-info-thanks\">¡Gracias por tu compra!</div>\n" +
                "            <div class=\"customer-info-invoice\">Informacion de compra</div>\n" +
                "        </div>\n" +
                "        <div class=\"order-info\">\n" +
                "            <div class=\"order-header\">\n" +
                "                <div class=\"order-field\">\n" +
                "                    <div class=\"order-field-title2\">Pedido No.: " + order.getId() + "</div>\n" +
                "                </div>\n" +
                "                <div class=\"order-field\">\n" +
                "                    <div class=\"order-field-title\">Dirección de entrega:</div><div class=\"order-field-detail\">" + order.getAddress() + "</div>\n" +
                "                </div>\n" +
                "                <div class=\"order-field\">\n" +
                "                    <div class=\"order-field-title\">Barrio:</div><div class=\"order-field-detail\">" + order.getZone() + "</div>\n" +
                "                </div>\n" +
                "                <div class=\"order-field\">\n" +
                "                    <div class=\"order-field-title\">Detalles:</div><div class=\"order-field-detail\">" + order.getAddressDetails() + "</div>\n" +
                "                </div>\n" +
                "                <div class=\"order-field\">\n" +
                "                    <div class=\"order-field-title\">Total del pedido:</div><div class=\"order-field-detail\">$" + decimalFormat.format(total) + "</div>\n" +
                "                </div>\n" +
                "                <div class=\"order-field\">\n" +
                "                    <div class=\"order-field-title\">Medio de pago:</div><div class=\"order-field-detail\">EFECTIVO (CONTRAENTREGA)</div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "            <div class=\"order-detail-container\">\n" +
                "                <div class=\"order-detail-title-container\">\n" +
                "                    <div class=\"separator\"></div>\n" +
                "                    <div class=\"order-detail-title\">Detalle del pedido</div>\n" +
                "                    <div class=\"separator\"></div>\n" +
                "                </div>\n" +
                "                <table class=\"order-detail-table\">\n" +
                "                    <thead>\n" +
                "                        <th class=\"order-detail-table-f-th\">PLU</th>\n" +
                "                        <th>Codigo de barras</th>\n" +
                "                        <th>Descripción</th>\n" +
                "                        <th>U.M.</th>\n" +
                "                        <th>Cantidad</th>\n" +
                "                        <th>Precio und.</th>\n" +
                "                        <th class=\"order-detail-table-l-th\">Total</th>\n" +
                "                    </thead>\n" +
                "                    <tbody>\n" +
                msgDetail +
                "                    </tbody>\n" +
                "                </table>\n" +
                "                <div class=\"order-detail-footer-container\">\n" +
                "                    <div class=\"separator\"></div>\n" +
                "                    <div class=\"order-detail-footer-total-container\">\n" +
                "                        <div class=\"order-detail-footer-total-title\">Total:</div><div class=\"order-detail-footer-total\">$" + decimalFormat.format(total) + "</div>\n" +
                "                    </div>\n" +
                "                    <div class=\"separator\"></div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "    <div class=\"footer\">\n" +
                "        <a href=\"https://app.listao.co\" class=\"footer-link\">app.listao.co</div>\n" +
                "    </div>\n" +
                "  </body>\n" +
                "</html>\n";

        MimeMessage message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress("servicioalcliente@listao.co", "LISTAO!")); // Quien envia el correo
            //message.addRecipient(Message.RecipientType.TO, new InternetAddress("alexanderlopezp@gmail.com"));
            message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(order.getCustomer().getEmail()));
            message.setSubject("Listao! Pedido No.: " + order.getId());

            MimeBodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            messageBodyPart.setContent(msg, "text/html; charset=utf-8");
            Multipart multipart = new MimeMultipart(); // Create a multipar message
            multipart.addBodyPart(messageBodyPart); // Set text message part

            // Send the complete message parts
            message.setContent(multipart);
            Transport t = session.getTransport("smtp");
            t.connect("servicioalcliente@listao.co", "listao1345");
            t.sendMessage(message, message.getAllRecipients());
            t.close();
        } catch (MessagingException ex) {
            Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public void sendOrderMail(Order order) {
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###.###");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
        Properties props = new Properties();
        props.setProperty("mail.smtp.host", "mail.eljardin.co"); // Nombre del host de correo, es smtp.gmail.com
        props.setProperty("mail.smtp.ssl.trust", "mail.eljardin.co");
        props.setProperty("mail.smtp.starttls.enable", "true"); // TLS si está disponible
        //props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.smtp.port", "25"); //Puerto del servidor para envio de correos
        props.setProperty("mail.smtp.user", "servicioalcliente@listao.co"); // Nombre del usuario
        props.setProperty("mail.smtp.auth", "true"); // Si requiere o no usuario y password para conectarse.
        Session session = Session.getDefaultInstance(props);
        session.setDebug(true);

        Double total = 0.0;
        String msgDetail = "";
        Boolean alternRowInd = false;
        for(OrderDetail orderDetail : order.getOrderDetails()){
            total = total + (orderDetail.getQty() * orderDetail.getDiscountPrice());
            String rowClass = "";
            if(alternRowInd){
                rowClass = "order-detail-table-row-b";
            }
            msgDetail = msgDetail +
                    "                        <tr class=\"" + rowClass + "\">\n" +
                    "                            <td>" + orderDetail.getProduct().getErpProduct().getSku() + "</td>\n" +
                    "                            <td>" + orderDetail.getProduct().getErpProduct().getEan() + "</td>\n" +
                    "                            <td>" + orderDetail.getProduct().getName() + "</td>\n" +
                    "                            <td>" + orderDetail.getMu() + "</td>\n" +
                    "                            <td>$" + decimalFormat.format(orderDetail.getDiscountPrice()) + "</td>\n" +
                    "                            <td>" + decimalFormat.format(orderDetail.getQty()) + "</td>\n" +
                    "                            <td>$" + decimalFormat.format(orderDetail.getDiscountPrice() * orderDetail.getQty()) + "</td>\n" +
                    "                        </tr>\n";
            alternRowInd = !alternRowInd;
        }

        String msg = "<!DOCTYPE html>\n" +
                "<html lang=\"es\">\n" +
                "  <head>\n" +
                "    <meta charset=\"utf-8\" />\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n" +
                "    <style>\n" +
                "        body {\n" +
                "            margin: 0;\n" +
                "            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',\n" +
                "                'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',\n" +
                "                sans-serif;\n" +
                "            -webkit-font-smoothing: antialiased;\n" +
                "            -moz-osx-font-smoothing: grayscale;\n" +
                "            background-color: white;\n" +
                "            color: black;\n" +
                "        }\n" +
                "\n" +
                "        .header {\n" +
                "            display: flex;\n" +
                "            justify-content: center;\n" +
                "            align-items: center;\n" +
                "            width: 100%;\n" +
                "            height: 100px;\n" +
                "            background-color: #00b9e2;\n" +
                "        }\n" +
                "\n" +
                "        .header-logo-container {\n" +
                "            \n" +
                "        }\n" +
                "\n" +
                "        .main-container {\n" +
                "            padding: 50px;\n" +
                "        }\n" +
                "\n" +
                "        .customer-info {\n" +
                "            display: flex;\n" +
                "            flex-direction: column;\n" +
                "            align-items: center;\n" +
                "            width: 100%;\n" +
                "            margin-top: 50px;\n" +
                "            font-size: 25px;\n" +
                "            gap: 20px;\n" +
                "        }\n" +
                "\n" +
                "        .customer-info-name {\n" +
                "            font-weight: bold;\n" +
                "        }\n" +
                "\n" +
                "        .customer-info-thanks {\n" +
                "            font-weight: bold;\n" +
                "            color: #00b9e2;\n" +
                "        }\n" +
                "\n" +
                "        .order-info {\n" +
                "            margin-top: 50px;\n" +
                "        }\n" +
                "\n" +
                "        .order-header {\n" +
                "            display: flex;\n" +
                "            flex-direction: column;\n" +
                "            font-size: 18px;\n" +
                "            gap: 10px;\n" +
                "        }\n" +
                "\n" +
                "        .order-field {\n" +
                "            display: flex;\n" +
                "        }\n" +
                "\n" +
                "        .order-field-title {\n" +
                "            font-weight: bold;\n" +
                "            width: 300px;\n" +
                "        }\n" +
                "\n" +
                "        .order-field-title2{\n" +
                "            font-weight: bold;\n" +
                "            font-size: 30px;\n" +
                "            width: 300px;\n" +
                "        }\n" +
                "\n" +
                "        .separator {\n" +
                "            width: 100%;\n" +
                "            border-bottom: solid 1px #565656;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-container {\n" +
                "            margin-top: 50px;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-title-container {\n" +
                "            display: flex;\n" +
                "            flex-direction: column;\n" +
                "            align-items: center;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-title {\n" +
                "            font-weight: bold;\n" +
                "            font-size: 20px;\n" +
                "            margin-top: 20px;\n" +
                "            margin-bottom: 20px;\n" +
                "        }\n" +
                "        \n" +
                "        .order-detail-table{\n" +
                "            border-top-left-radius: 5px;\n" +
                "            border-top-right-radius: 5px;\n" +
                "            margin-top: 20px;\n" +
                "            width: 100%;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-table th{\n" +
                "            background-color: #00b9e2;\n" +
                "            color: white;\n" +
                "            height: 30px;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-table td{\n" +
                "            text-align: center;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-table td{\n" +
                "            height: 30px;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-table-f-th{\n" +
                "            border-top-left-radius: 5px;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-table-l-th{\n" +
                "            border-top-right-radius: 5px;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-table-row-b {\n" +
                "            background-color: #00b9e222\n" +
                "        }\n" +
                "        \n" +
                "        .order-detail-footer-container {\n" +
                "            display: flex;\n" +
                "            flex-direction: column;\n" +
                "            margin-top: 20px;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-footer-total-container {\n" +
                "            display: flex;\n" +
                "            justify-content: right;\n" +
                "            font-size: 20px;\n" +
                "            margin-top: 20px;\n" +
                "            margin-bottom: 20px;\n" +
                "        }\n" +
                "\n" +
                "        .order-detail-footer-total-title {\n" +
                "            font-weight: bold;\n" +
                "            width: 100px;\n" +
                "        }\n" +
                "\n" +
                "        .footer {\n" +
                "            display: flex;\n" +
                "            justify-content: center;\n" +
                "            align-items: center;\n" +
                "            width: 100%;\n" +
                "            height: 70px;\n" +
                "            background-color: #00b9e2;\n" +
                "        }\n" +
                "\n" +
                "        .footer-link {\n" +
                "            color: white;\n" +
                "            font-weight: bold;\n" +
                "            font-size: 30px;\n" +
                "            text-decoration: none;\n" +
                "        }\n" +
                "    </style>\n" +
                "    <title>Listao</title>\n" +
                "  </head>\n" +
                "  <body>\n" +
                "    <div class=\"header\">\n" +
                "        <div class=\"header-logo-container\">\n" +
                "            <img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAAA+CAYAAAAClQafAAAAAXNSR0IArs4c6QAAErdJREFUeAHtnQuYVdV1x/edJ8NDZgbUQYaAJRBQ1NRHHgQCApr6iDWaF0nTElEr2qix5muMTYk2SY1fqWmMiCZolGisGkRapP0ERxEDUtSKTREQ1OCAojxUBgbmcfv7D3MnZ+6ce2fvc+6duffOWd/3n3PO3mutvfa6e529z977nImZZFpZP5GkWca0nslxuImbimSWPLo+bGKxncbEnzMx84CZNuKJPLI9MjXyQMY8EOvQtH7PYLPvwEKC4uKOtEI7iZlnjamYaaYPrS+0qkX1iTyQzgNHAr1ub6Vp3b+a3vvEdMwFkRczb5uS0olmSs3rBVGfqBKRByw8UNTG09pwf58IclU2bmpMc/MSUxcvsfBPxBJ5oCA8UGTq6meYePzzBVEb20rE4yeblvrZtuwRX+SBfPdAkWlpnZXvlQhof1+td0B3RWL57IEiZqUn5XMFAtseM5806+OlgeUjwcgDeeQBnlPjx+WRvZkzNR6PmYYdNSjcHkZpPB7/JfITAujYGIvFvmkrRznD4P1bMAOMAIfAJvAouAtdzRwjijzg64ESJqf6bq/WXFLm6xW3xMmwj3UTaeM+2laGIL8M3ltBZZKMgn8quAyeSwn29Un50WXkgTYPHJl1j5yRsx4ggK/BuLtBcpB7bT6Fi2fgHedNjM4jDyQ8EAV6whM5eCRwT8Ksn1ia1h++B5HJxCjFssiILV88EAV6bv9SN2FeuYOJfwrvlx34I9Y+4oEo0HP7h9bEmyud5SoQ8Re+B6LdYTn6GzMEH4BpgwKYp5WEvKf2+v8ZFSlnkvHBvK9QL1cg6tHD/wAt4VV01UDjbiBVS2iutMdVIFf4Ce6jwWzw79j0HtDS4ddyxb58tiPq0cP/ehoqV4GPg0Xh1XXSsJqr6Z1Sur94tnuW3OEgqEdjzYXt0CvSUeeThZ8nCvSQTqXnrUdFPQ1Ws96ZpltQOA388XXi9CX8gewH0rP0fi6+0nLgF4ECfELvW1T4FoQKdN16x/QvMYOKbduhnUP3NbearQdb2MvjT/0oeGS/EvP6wWZzOBWTv2hepXITWUFQ3IbR11kY3grPN5B534K3t1k0UhnY20b0pfIDB/oZg0rNwycNMaMqAqtI6+eNDU3m4g27zcYDnXd2Xnpcf/OvYytN/+Iis6+p1czeuNcsfvdgWl15nnkD9uueenX70a86ep69iiBf5ZeZg2nFOWhTQZukBuRM6r+zGeQyaPyAUrPoxOpOtp0woMQsGFfVFuTKqCwtgqfK1JQFqkYn3bl6QfAeBt/Gvk+B5aDRY+suzueDcfA87EmPTiMPdPJAoO74I/2Ks9aTe6077agyM6CId09aj4zPJw4uM8Wxzo8J6tlPG1Rmlu32tn+vlsI4J5D/m5qcy1BevaH2yTeStq8wateztcCH6hkqwVFAqyb7BfzZxLEgKVCg7zjUYvbzHD2wJLs9aX1jS0eQy/ubk4bxSuNHM1t80pVXiERjVMN8O1t1w5/6GOghytEzf0EQddLQ8GxwDjgV6CWkLluF4dtBut4IfBU8A57ED72yXIktsm8o0BLrHuwINRsVKNCbKHLOpn1m4fgqU0aPmw060NJqrnh1byfVq/YdNgvrG8zs4dpLcoR+/MaHZjOTcoVE/MgzqE9tgDq9RYNYYStHOaXwalPKBeATQAHQDzSRp9WEN8F/gd+g9w2OHUS+bgjng1QN4H+Q2SwBeI/hcK7OIfGrXFsaifwsH+Yt6H/OJ70tCRlN9v0VmAn02GMzL3AcfMKZYA5oQc8ajreD31JeVvZMUIZGaNPAVDAZjAAabSSoGZ53udgOVoM68Cz2WE+8xsyK7YHvFKMris2UynIzKMM9uybZntp7yGxn5OBHn2ZIf8LAEvPiB03mpf0hRluxso+a6cdu9SvDNY0fQoHyvIPcNn6o0X786FKAjPHL6yZtKzo/2g2PAk/7568ENwCb12XVRhTwmvDbxlE6FOTa2JKKfgnvZcqE95sc7knFGDB9NfoVFJ2Iso4n4WpwCfAGSye+ABdqJzdS5r8FkPUVwdbTydCKypeAa6er3u0RMA+bXuCYllyVd1KmJbCtBw90SuuJizUfHDZCRO4eoHHpBrIEnOAgrV5YPf8ryF9Lw/oF5yMd5LPOil3DKOQHQAEeql0j70e6KT9EOZ/j+C180ODHZJOGDvlOPjzLhj8Fj+qo0cpM9D3F8Qps2pKCN9qFlMoxhZhOg9Brr+uAS5B7XdGfi7vQ8zWO6jlziZZizOUgG0HuradGJ6vwgXzhTMhJfgMIE+TJ5WrY/xK6dZPzpSLf1Cix4DxAIxhMpR4Dmm0OQ+rd7wXqTXKJtPTYU6QJvfvwqXxhTfDPg1mPMJl8pEiUr4mrhZRxRyLBewx996vgVjGAJa6epkMsuX3YokfHiCw98C34fOcELOW9bJoR1qRVLtH9GPP9HjRIW3ivAj+3KZMA/BF8eh7PNl1JWe8xjJ/rLShwoGsi7p7x1WZSZZkpSlrb9haQzfPfMxF3OTPzv3s/el5P52d+eN2Jr0zHk+95NOzXqOdq6jGpB+tyHWXOp+zWdGXCcwX530vHk+G8f6DMN7BLI682CtwV/5btr5+tKu+1IJf1Jw4sNUtPGWKqSpxGUEdq3rf+1lJdTVbZkpYy1Ft9BIwDfw60xNTzM68U6kC/SsPbQt6LQBORC8BtQIGwDFgvU8HrpeO50OpDSiLg5PefpGRIn6EJP82uB6F/puzqhGCgHr22vNicwm60XKAhpcXmU4PLzfLC2hn3G3w7FtSAqSAsqUG6kD4fPd8jsInzpTScmzhqqPpVT57N6cswPdTOqLvyl4BtJ/MevCvaZb2Hdd6L9nMtN/0MJCbKDnG+GKjsZ6iTb0BTL8XBhWAhcH1+1hB+KUhFPyXDVedeZGYBLV8q0HSuG1MFsCUF+Q/A1RIIFOi7m1rMYZ6Rs7VZRoa5kHbqFRLRINuer2iAU6mXEJYUXC60y48Zu3Zj09fJ2w8u9ePxS0PuRdJnJvLQcQHnto32FeQ7ZBM6/I7wfYDuxeSdCxQYd8pmP15vGjzqNR9FdgfH57x5Fucpb6LoG4/8ly10eFn0GPB5bErYoZuVVjq2c9Tow4XmIHcLunbY3lU7KT+IKd/f5ntz7MTXExeLdjaYl8NsmukJI3u/jLcdTbieBqIergvRaNQQNbH3TpfM3EiYgxmjsPOHwDfIqZtvu4f/d8gq2F1oVBpm15GPVD2AHYkg71BN2hNc6CbmQomRSrAeXSXd+uZ+s5ZJsBnV/cxRGX4f3aYmjYwo1rJpZsm7jTbsfZ3nNRyg4WCVpSOOgu8xAuIljveBR2hoHQHAeSN5GpKeB1LRC6kyspmObRptdBB2ap5BPfxkcCo4GlSRrnmInWAD0BD5oXbZfZy7rCgchy7etfLdHvsVdLnS/WkEFpF3UZp8v6wvkDg/1BZYP615lZajW2ATPqQBTeW8LnFtcUy5BRZdakDfsNDhx6JeXL2depTHaNRvcAxM2KJJPduhex3lTXMtjDI+jcx3wAWg2EJevb9mxq8HYyz4vSz6gGWnpR/K15B+m5fJ4lw3nlp0yd9dCJ3lJOqxSjdiW9JjSaXvEMZWQ8SXVx74J6xtCWix2skk8C/gdRrcOnANODagvqyJYVN/8DMK0PBXvZlNkMueIeAu4BrkkvUjjSRcaUOqIJci8vS8/oqjUg3fR0aB7ui1fGWnkWzE9h9myP4z0KOhu76V9wT4TIb0hlKDHZUoWAU0h+A6ARmqbB/hWp+07pLqu2MgX72+K9VGge7qsvzmvwnzvctmYWuj3vIcsJog0zP9x8IqDCpP2QOQfRKcFlRHhuWyFeiuE6uqVq269UA0lM84zRsz2JzFZFymPw5pY9AhPjihycDrNr9fcO+j29Q/CA+9ehy5qwiKZzjeDQYH0ZNC5kLSz0f3dylnXgqebCbfjPLTs1mAo27f52xHHX7sQWaf+QhbQHrkpGoztapfQOnwYvqqwHlDK8wEvi134tp3On2JJrz2wtZAID5MQK6jlhrKa523NEM1VnvSjixNTv04Qzq7VUN5J8F0TbeMPcsQpOcdbmFijQVPMsuuQEN3fYyxN4PcW4uRfIV2IvvtI3LzAIGovdB/gdRIoN5wu5uGtNw3E3ya9e4pupSCbCfdEjZt5eRecAt4AGR6X0CQQLdZ1uu5QG/grbFWhs65Qh82544tueITWzsI9p1gLtAssXrGvwN1QDO8QUlBNzeNsMsPlpaXG4o6K41KXOhOmMdT50vADUA3vFHgdpApCjJpZjPjPzqAge8EGrrr9dCfv7XfXD1iUIAyMyuyck+jeT762kxapxIM02BI9/KFPj6onWT/C59wKzJ6LpsIpgMtU40HLjQDHcei06+n1E2kv6Wy5m74hpHv0stp7/y3sUsbZjqIa20C0vD/YmDTs3bIpjj5P9LVq7vYNhobTseW9X46yZtA+vF+eWnSVN9NgQJdSq9lEuwFvtl2VnXmvxmXxuiOLL2Prsm4+dxw0t7yOyT69Ik2jaR9hqURPU4D61ijVcNH5ql23Ei+dpf9GtjurlOvrobpF+gHHPR093jpOrv9e+rmO1ohnWrGffOw14nQxaA3/hhCc5wEj3zQ0jfQ0fNFR11iX4YtLYEDXcF1/9sH2hCg8Egk9zywgIaplyn2+JlGutbL1XAv8ctPkZYqCBtS8PslD/FL9KS57BKTWCqb9BFLTfZomS5T9CiKXAN9Fnbchr+3eY0gTXsE/tqbZnm+VHyBA92ykIJn4wfQ7LKGeq67xGqQ/ZXHQdpa+riuSdd690hgMwsrkQQN8+jUdth/TGRYHDVM34j89Ry1t109egeRrqG2eFwo1U68d1Ey1lLRyZQ9Bnu2JPi51n71E0jTMuGORLrlUcNjfUhxgZefNN1Q7gPHeNMtz/WWWGIk8Ca6l7fLPc3xRXBq+7XNYSBMy9B3Pno0Yaj2MJiDbhoujwESlfx/6CTa6x7yc8/8CLvxY7WcGZLu4If9G+lA51scXIM8uXjNqrc9z6FPu9jSDt2ThA9y/TR4Cah+CoKLwDjgQpOxYXWyAPYsJk3P/bb0KoyaGVdvq5vNZ8AadE9GlyaKPgCutAwBPZocBh8HmtDLxKSTtrGegq42wr5PcLIGdPcIckTgj3+bOJV9Gv1MBUHa2BewZQmyUY8uJ0TUxQMVpJzTji6ZlgkKPvVmfrTZLzFNmm4wvqMTGvKHBNMq8j+bRt4v6zwShawS9um9gF9QiOuwW3sbPhfCuJWJIJcO17tMiHIj0T7mAb1XrUk3P3rBLzFE2j0hZHtC9FoKWdETBbWXsYnjV73lRYHu9UZ0nikPvIOiuWmU6dlaQ+ZM0YMoWp8pZZnWww2vEZ0XgKczrdtHnx77zqbM97x5UaB7vdF3zzVp0zY/kAEXqBefSUPThJsvkbeLDD2nZ4TQp+fZr4Mgz+p+NqQaifjxWqVho+Y9zgd3WwkEY9J8yCTK+kOyeBToyR7pm9daW76Dql8JwvS06smnoavOwo03wrPHgs+KhTL13D8Z1FsJpGZ6mSztUNPGoYwSNjYAPaufDboEY4jCdKP7ezAF/W/66YkC3c8rfS9Nz3T6sMGdHE4Gmu11Id0cbgcfQ8fzNoLwbYNPG0A6DTFtZFPxoHMDeaeDRUBbPVxpJQJT0bOD43ddhW350f8kvGPBLBBmvmIv8reAUej8EWjl3JdKfFP7SmJZoMaQ7J3vkHBscmKAa28v+D3khwfQ4RXx9hhan2/2ZiadP5i4prEo6KczU6wlJ03oXAT+BBQDL73PxTqgZapfI7fbm2lzjkwd5YyHVzPqF4OjLeQ0rNZylda8uxA6te30L9H7U46Xga+AKpCO1pJ5E7L/mWDiXGvZV3BdnUizPL5mw4d+rburDvrXTro5zQAakUwElcCPFMivAM1xrALL0WP1mKF1dP1ArpVBpACofFC1mVypu2JEaTxAQ1SQ14BhoBFoyK2XYYL0moh2JcrQ6PI0oMAf0o5yjrqh7ASaZNLwXPsDrMtFr5apJgAF0yigoFcHp3mCV4GWoaQ/Jwh7YxiiQJcPhoIyIH8rTndjq0ZPzhQzK99ayw6NTzpL5ruAJoum1x6T79WI7I88YOMB7qLxJTaMhccTf7zw6hTVKPKAvweKTNHABUzDOD9f+avLk9SYaWLwdmueWBuZGXkgtAeKzJlV+0xR/HKmXK2fe0KX2tsK4rEbzJQRW3rbjKj8yAM95QFNgLDyOWIxvfpszgI96LfpyIc/bTezorlmRu28fDA3sjHyQKY8cCTQpW368Hv5ewY9+/LC7N1jaxi5TDMzht+cKedFeiIP5IsHNJXflep2sZTSNIXZ+OGsNFd0ZciTFC1FtLbuNKWlz5kpNa/nidWRmZEHMu6B/wfdmyQLx31Y2wAAAABJRU5ErkJggg==\" alt=\"logo listao\">\n" +
                "        </div>\n" +
                "    </div>\n" +
                "    <div class=\"main-container\">\n" +
                "        <div class=\"order-info\">\n" +
                "            <div class=\"order-header\">\n" +
                "                <div class=\"order-field\">\n" +
                "                    <div class=\"order-field-title2\">Pedido No.: " + order.getId() + "</div>\n" +
                "                </div>\n" +
                "                <div class=\"order-field\">\n" +
                "                    <div class=\"order-field-title\">Nombre del cliente:</div><div class=\"order-field-detail\">" + order.getCustomer().getName() + " " + order.getCustomer().getLastnames() + "</div>\n" +
                "                </div>\n" +
                "                <div class=\"order-field\">\n" +
                "                    <div class=\"order-field-title\">Dirección de entrega:</div><div class=\"order-field-detail\">" + order.getAddress() + "</div>\n" +
                "                </div>\n" +
                "                <div class=\"order-field\">\n" +
                "                    <div class=\"order-field-title\">Detalles:</div><div class=\"order-field-detail\">" + order.getAddressDetails() + "</div>\n" +
                "                </div>\n" +
                "                <div class=\"order-field\">\n" +
                "                    <div class=\"order-field-title\">Barrio:</div><div class=\"order-field-detail\">" + order.getZone() + "</div>\n" +
                "                </div>\n" +
                "                <div class=\"order-field\">\n" +
                "                    <div class=\"order-field-title\">Teléfono de contacto:</div><div class=\"order-field-detail\">" + order.getTelephoneNumber() + "</div>\n" +
                "                </div>\n" +
                "                <div class=\"order-field\">\n" +
                "                    <div class=\"order-field-title\">Total del pedido:</div><div class=\"order-field-detail\">$" + decimalFormat.format(total) + "</div>\n" +
                "                </div>\n" +
                "                <div class=\"order-field\">\n" +
                "                    <div class=\"order-field-title\">Medio de pago:</div><div class=\"order-field-detail\">EFECTIVO (CONTRAENTREGA)</div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "            <div class=\"order-detail-container\">\n" +
                "                <div class=\"order-detail-title-container\">\n" +
                "                    <div class=\"separator\"></div>\n" +
                "                    <div class=\"order-detail-title\">Detalle del pedido</div>\n" +
                "                    <div class=\"separator\"></div>\n" +
                "                </div>\n" +
                "                <table class=\"order-detail-table\">\n" +
                "                    <thead>\n" +
                "                        <th class=\"order-detail-table-f-th\">PLU</th>\n" +
                "                        <th>Codigo de barras</th>\n" +
                "                        <th>Descripción</th>\n" +
                "                        <th>U.M.</th>\n" +
                "                        <th>Cantidad</th>\n" +
                "                        <th>Precio und.</th>\n" +
                "                        <th class=\"order-detail-table-l-th\">Total</th>\n" +
                "                    </thead>\n" +
                "                    <tbody>\n" +
                msgDetail +
                "                    </tbody>\n" +
                "                </table>\n" +
                "                <div class=\"order-detail-footer-container\">\n" +
                "                    <div class=\"separator\"></div>\n" +
                "                    <div class=\"order-detail-footer-total-container\">\n" +
                "                        <div class=\"order-detail-footer-total-title\">Total:</div><div class=\"order-detail-footer-total\">$" + decimalFormat.format(total) + "</div>\n" +
                "                    </div>\n" +
                "                    <div class=\"separator\"></div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "    <div class=\"footer\">\n" +
                "        <a href=\"https://app.listao.co\" class=\"footer-link\">app.listao.co</div>\n" +
                "    </div>\n" +
                "  </body>\n" +
                "</html>\n";

        MimeMessage message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress("servicioalcliente@listao.co", "LISTAO!")); // Quien envia el correo
            //message.addRecipient(Message.RecipientType.TO, new InternetAddress("alexanderlopezp@gmail.com"));
            message.addRecipients(Message.RecipientType.TO, InternetAddress.parse("alopez@listao.co"));
            message.setSubject("PEDIDO No.: " + order.getId());

            MimeBodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            messageBodyPart.setContent(msg, "text/html; charset=utf-8");
            Multipart multipart = new MimeMultipart(); // Create a multipar message
            multipart.addBodyPart(messageBodyPart); // Set text message part

            // Send the complete message parts
            message.setContent(multipart);
            Transport t = session.getTransport("smtp");
            t.connect("servicioalcliente@listao.co", "listao1345");
            t.sendMessage(message, message.getAllRecipients());
            t.close();
        } catch (MessagingException ex) {
            Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
