package co.aldetec.easyshopbackend.v1.misc;

import co.aldetec.easyshopbackend.v1.annotations.JsonExclude;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CustomGson {

    public CustomGson() {
    }

    public static Gson getCustomGson() {
        ExclusionStrategy strategy = new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes field) {
                return field.getAnnotation(JsonExclude.class) != null;
            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        };

        Gson gson = new GsonBuilder().disableHtmlEscaping()
                .setExclusionStrategies(strategy)
                .create();
        return gson;

    }
}
