package co.aldetec.easyshopbackend.v1.misc;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    static public Date getActualDate(){
        Calendar calendar = Calendar.getInstance();
        Date actualDate = new Date();
        calendar.setTime(actualDate);
        calendar.add(Calendar.HOUR, -5);
        return calendar.getTime();
    }

    static public Date getCurrentDate() {
        SimpleDateFormat zona = new SimpleDateFormat("zzz");
        Calendar calendar = Calendar.getInstance();
        Date actualDate = new Date();
        calendar.setTime(actualDate);
        calendar.add(Calendar.HOUR, -5);
        return calendar.getTime();
    }

    static public String getCurrentInvoiceDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat zona = new SimpleDateFormat("zzz");
        Calendar calendar = Calendar.getInstance();
        Date actualDate = new Date();
        calendar.setTime(actualDate);
        calendar.add(Calendar.HOUR, -5);
        return simpleDateFormat.format(calendar.getTime());
    }

}
