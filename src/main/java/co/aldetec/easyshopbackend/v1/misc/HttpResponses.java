package co.aldetec.easyshopbackend.v1.misc;

import org.bson.Document;

public class HttpResponses {

    public static Document getResponse(Integer status, String code, String msg){
        Document d = new Document();
        d.append("status", status);
        d.append("code", code);
        d.append("message", msg);
        return d;
    }
}
