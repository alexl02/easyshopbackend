package co.aldetec.easyshopbackend.v1.persistence.dao;

import co.aldetec.easyshopbackend.v1.persistence.entities.Branch;
import co.aldetec.easyshopbackend.v1.persistence.entities.Company;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
@Transactional
public class BranchDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public List<Branch> findAll() throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select b from Branch b");
            return query.list();
        } catch (Exception ex) {
            Logger.getLogger(BranchDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public List<Branch> findByCompany(Integer companyId) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select b from Branch b where b.company.id = :companyId");
            query.setParameter("companyId", companyId);
            return query.list();
        } catch (Exception ex) {
            Logger.getLogger(BranchDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public Branch findById(Integer id) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select b from Branch b where b.id = :id");
            query.setParameter("id", id);
            return (Branch) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(BranchDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public Branch findByCompanyAndErpBranchId(Company company, String erpBranchId) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select b from Branch b where b.company = :company and b.erpBranchId = :erpBranchId");
            query.setParameter("company", company);
            query.setParameter("erpBranchId", erpBranchId);
            return (Branch) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(BranchDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }
}
