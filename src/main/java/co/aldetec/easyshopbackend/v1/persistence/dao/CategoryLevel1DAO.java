package co.aldetec.easyshopbackend.v1.persistence.dao;

import co.aldetec.easyshopbackend.v1.persistence.entities.CategoryLevel1;
import co.aldetec.easyshopbackend.v1.persistence.entities.Company;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
@Transactional
public class CategoryLevel1DAO {
    @Autowired
    private SessionFactory sessionFactory;

    public List<CategoryLevel1> findAll(Company company) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from CategoryLevel1 c where c.company = :company and c.state = :state");
            query.setParameter("company", company);
            query.setParameter("state", true);
            return query.list();
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel1DAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public CategoryLevel1 findById(Company company, Integer id) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from CategoryLevel1 c where c.company = :company and c.id = :id");
            query.setParameter("company", company);
            query.setParameter("id", id);
            return (CategoryLevel1) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel1DAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public CategoryLevel1 findByAlterCode(Company company, String alterCode) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from CategoryLevel1 c where c.company = :company and c.alterCode = :alterCode");
            query.setParameter("company", company);
            query.setParameter("alterCode", alterCode);
            return (CategoryLevel1) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel1DAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public CategoryLevel1 findBySearchableWords(Company company, String searchableWords) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from CategoryLevel1 c where c.company = :company and c.searchableWords like %:searchAbleWords%");
            query.setParameter("company", company);
            query.setParameter("searchableWords", searchableWords);
            return (CategoryLevel1) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel1DAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public CategoryLevel1 save(CategoryLevel1 categoryLevel1) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.save(categoryLevel1);
            Query query = session.createSQLQuery("SELECT currval(pg_get_serial_sequence('category_level_1','id'))");
            Integer id = ((BigInteger) query.uniqueResult()).intValue();
            query = session.createQuery("select c from CategoryLevel1 c where c.id = :id");
            query.setParameter("id", id);
            CategoryLevel1 categoryLevel1Saved = (CategoryLevel1) query.uniqueResult();
            session.getTransaction().commit();
            return categoryLevel1Saved;
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel1DAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public CategoryLevel1 update(CategoryLevel1 categoryLevel1) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.update(categoryLevel1);
            Query query = session.createQuery("select c from CategoryLevel1 c where c.id = :id");
            query.setParameter("id", categoryLevel1.getId());
            CategoryLevel1 categoryLevel1Saved = (CategoryLevel1) query.uniqueResult();
            session.getTransaction().commit();
            return categoryLevel1Saved;
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel1DAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public void updateMany(List<CategoryLevel1> categoryLevel1List) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            for( CategoryLevel1 categoryLevel1New : categoryLevel1List ){
                Query query = session.createQuery("select c from CategoryLevel1 c where c.company = :company and c.alterCode = :alterCode");
                query.setParameter("company", categoryLevel1New.getCompany());
                query.setParameter("alterCode", categoryLevel1New.getAlterCode());
                CategoryLevel1 categoryLevel1 = (CategoryLevel1) query.uniqueResult();

                if(categoryLevel1 == null){
                    categoryLevel1 = new CategoryLevel1();
                    categoryLevel1.setCompany(categoryLevel1New.getCompany());
                    categoryLevel1.setAlterCode(categoryLevel1New.getAlterCode());
                }
                categoryLevel1.setName(categoryLevel1New.getName());
                categoryLevel1.setImgUrl(categoryLevel1New.getImgUrl());
                categoryLevel1.setSearchableWords(categoryLevel1New.getSearchableWords());
                categoryLevel1.setState(categoryLevel1New.getState());

                session.saveOrUpdate(categoryLevel1);
            }
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel1DAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex);
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public void delete(CategoryLevel1 categoryLevel1) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.delete(categoryLevel1);
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel1DAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }
}
