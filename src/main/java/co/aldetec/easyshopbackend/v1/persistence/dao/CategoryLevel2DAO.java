package co.aldetec.easyshopbackend.v1.persistence.dao;

import co.aldetec.easyshopbackend.v1.persistence.entities.CategoryLevel2;
import co.aldetec.easyshopbackend.v1.persistence.entities.Company;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
@Transactional
public class CategoryLevel2DAO {

    @Autowired
    private SessionFactory sessionFactory;

    public List<CategoryLevel2> findAll(Company company) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from CategoryLevel2 c where c.categoryLevel1.company = :company");
            query.setParameter("company", company);
            return query.list();
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel2DAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public CategoryLevel2 findById(Company company, Integer id) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from CategoryLevel2 c where c.company = :company and c.categoryLevel1.id = :id");
            query.setParameter("company", company);
            query.setParameter("id", id);
            return (CategoryLevel2) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel2DAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public CategoryLevel2 findByAlterCode(Company company, String alterCode) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from CategoryLevel2 c where c.categoryLevel1.company and c.alterCode = :alterCode");
            query.setParameter("company", company);
            query.setParameter("alterCode", alterCode);
            return (CategoryLevel2) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel2DAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public List<CategoryLevel2> findByCategoryLevel1Id(Company company, Integer id) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from CategoryLevel2 c where c.categoryLevel1.company = company and c.categoryLevel1.id = :id");
            query.setParameter("company", company);
            query.setParameter("id", id);
            return query.list();
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel2DAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public List<CategoryLevel2> findBySearchableWords(Company company, String searchableWords) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from CategoryLevel2 c where c.categoryLevel1.company = company and c.searchableWords = :searchableWords");
            query.setParameter("company", company);
            query.setParameter("searchableWords", searchableWords);
            return query.list();
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel2DAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public CategoryLevel2 save(CategoryLevel2 categoryLevel2) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.save(categoryLevel2);
            Query query = session.createSQLQuery("SELECT currval(pg_get_serial_sequence('category_level_2','id'))");
            Integer id = ((BigInteger) query.uniqueResult()).intValue();
            query = session.createQuery("select c from CategoryLevel2 c where c.id = :id");
            query.setParameter("id", id);
            CategoryLevel2 categoryLevel2Saved = (CategoryLevel2) query.uniqueResult();
            session.getTransaction().commit();
            return categoryLevel2Saved;
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel2DAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public CategoryLevel2 update(CategoryLevel2 categoryLevel2) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.update(categoryLevel2);
            Query query = session.createQuery("select c from CategoryLevel2 c where c.id = :id");
            query.setParameter("id", categoryLevel2.getId());
            CategoryLevel2 categoryLevel2Saved = (CategoryLevel2) query.uniqueResult();
            session.getTransaction().commit();
            return categoryLevel2Saved;
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel2DAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public void updateMany(List<CategoryLevel2> categoryLevel2List) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            for( CategoryLevel2 categoryLevel2New : categoryLevel2List ){
                Query query = session.createQuery("select c from CategoryLevel2 c where c.categoryLevel1 = :categoryLevel1 and c.alterCode = :alterCode");
                query.setParameter("categoryLevel1", categoryLevel2New.getCategoryLevel1());
                query.setParameter("alterCode", categoryLevel2New.getAlterCode());
                CategoryLevel2 categoryLevel2 = (CategoryLevel2) query.uniqueResult();

                if(categoryLevel2 == null){
                    categoryLevel2 = new CategoryLevel2();
                    categoryLevel2.setCategoryLevel1(categoryLevel2New.getCategoryLevel1());
                    categoryLevel2.setAlterCode(categoryLevel2New.getAlterCode());
                }

                categoryLevel2.setName(categoryLevel2New.getName());
                categoryLevel2.setSearchableWords(categoryLevel2New.getSearchableWords());
                categoryLevel2.setState(categoryLevel2New.getState());
                session.saveOrUpdate(categoryLevel2);
            }
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel2DAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public void delete(CategoryLevel2 categoryLevel2) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.delete(categoryLevel2);
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(CategoryLevel2DAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }
    
}
