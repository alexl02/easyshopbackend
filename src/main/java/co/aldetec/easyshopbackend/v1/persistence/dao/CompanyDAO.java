package co.aldetec.easyshopbackend.v1.persistence.dao;

import co.aldetec.easyshopbackend.v1.persistence.entities.Company;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
@Transactional
public class CompanyDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public List<Company> findAll() throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from Company c");
            return query.list();
        } catch (Exception ex) {
            Logger.getLogger(CompanyDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public Company findById(Integer id) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from Company c where c.id = :id");
            query.setParameter("id", id);
            return (Company) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(CompanyDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public Company findByNit(String nit) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from Company c where c.nit = :nit");
            query.setParameter("nit", nit);
            return (Company) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(CompanyDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public Company findByApiKey(String apiKey) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from Company c where c.apiKey = :apiKey");
            query.setParameter("apiKey", apiKey);
            Company company = (Company) query.uniqueResult();
            return company;
        } catch (Exception ex) {
            Logger.getLogger(CompanyDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public Company save(Company company) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.save(company);
            Query query = session.createSQLQuery("SELECT currval(pg_get_serial_sequence('company','id'))");
            Integer id = ((BigInteger) query.uniqueResult()).intValue();
            query = session.createQuery("select c from Company c where c.id = :id");
            query.setParameter("id", id);
            Company companySaved = (Company) query.uniqueResult();
            session.getTransaction().commit();
            return companySaved;
        } catch (Exception ex) {
            Logger.getLogger(CompanyDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public Company update(Company company) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.update(company);
            Query query = session.createQuery("select c from Company c where c.id = :id");
            query.setParameter("id", company.getId());
            Company companySaved = (Company) query.uniqueResult();
            session.getTransaction().commit();
            return companySaved;
        } catch (Exception ex) {
            Logger.getLogger(CompanyDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public void delete(Company company) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.delete(company);
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(CompanyDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }
}
