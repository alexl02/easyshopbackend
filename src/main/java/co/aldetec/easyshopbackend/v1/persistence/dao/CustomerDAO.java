package co.aldetec.easyshopbackend.v1.persistence.dao;

import co.aldetec.easyshopbackend.v1.persistence.entities.Customer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
@Transactional
public class CustomerDAO {
    @Autowired
    private SessionFactory sessionFactory;

    public List<Customer> findAll() throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from Customer c");
            return query.list();
        } catch (Exception ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public Customer findById(Integer id) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from Customer c where c.id = :id");
            query.setParameter("id", id);
            return (Customer) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public Customer findByEmail(String email) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from Customer c where c.email = :email");
            query.setParameter("email", email);
            return (Customer) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public Customer findByCelNumber(String celNumber) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from Customer c where c.celNumber = :celNumber");
            query.setParameter("celNumber", celNumber);
            return (Customer) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public Customer findByEmailAndPassword(String email, String password) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select c from Customer c where c.email = :email and c.password = :password");
            query.setParameter("email", email);
            query.setParameter("password", password);
            return (Customer) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public Customer save(Customer customer) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.save(customer);
            Query query = session.createSQLQuery("SELECT currval(pg_get_serial_sequence('customer','id'))");
            Integer id = ((BigInteger) query.uniqueResult()).intValue();
            query = session.createQuery("select c from Customer c where c.id = :id");
            query.setParameter("id", id);
            Customer customerSaved = (Customer) query.uniqueResult();
            session.getTransaction().commit();
            return customerSaved;
        } catch (Exception ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public Customer update(Customer customer) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.update(customer);
            Query query = session.createQuery("select c from Customer c where c.id = :id");
            query.setParameter("id", customer.getId());
            Customer customerSaved = (Customer) query.uniqueResult();
            session.getTransaction().commit();
            return customerSaved;
        } catch (Exception ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public void delete(Customer customer) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.delete(customer);
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }
}
