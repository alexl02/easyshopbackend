package co.aldetec.easyshopbackend.v1.persistence.dao;

import co.aldetec.easyshopbackend.v1.persistence.entities.Customer;
import co.aldetec.easyshopbackend.v1.persistence.entities.CustomerSession;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
@Transactional
public class CustomerSessionDAO {
    @Autowired
    private SessionFactory sessionFactory;

    public CustomerSession findById(Integer id) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select cs from CustomerSession cs where cs.id = :id");
            query.setParameter("id", id);
            return (CustomerSession) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(CustomerSessionDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public List<CustomerSession> findByCustomer(Customer customer) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select cs from CustomerSession cs where cs.customer = :customer");
            query.setParameter("customer", customer);
            return query.list();
        } catch (Exception ex) {
            Logger.getLogger(CustomerSessionDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public CustomerSession save(CustomerSession customerSession) throws Exception {
        Session session = this.sessionFactory.openSession();
        CustomerSession customerSessionSaved = null;
        try{
            session.beginTransaction();
            session.save(customerSession);
            Query query = session.createSQLQuery("SELECT currval(pg_get_serial_sequence('customer_session','id'))");
            Integer id = ((BigInteger) query.uniqueResult()).intValue();
            query = session.createQuery("select cs from CustomerSession cs where cs.id = :id");
            query.setParameter("id", id);
            customerSessionSaved = (CustomerSession) query.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(CustomerSessionDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
        return customerSessionSaved;
    }

    public CustomerSession update(CustomerSession customerSession) throws Exception {
        Session session = this.sessionFactory.openSession();
        CustomerSession customerSessionSaved = null;
        try{
            session.beginTransaction();
            session.update(customerSession);
            Query query = session.createQuery("select cs from CustomerSession cs where cs.id = :id");
            query.setParameter("id", customerSession.getId());
            customerSessionSaved = (CustomerSession) query.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(CustomerSessionDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
        return customerSessionSaved;
    }

    public void delete(CustomerSession customerSession) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.delete(customerSession);
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }
}
