package co.aldetec.easyshopbackend.v1.persistence.dao;

import co.aldetec.easyshopbackend.v1.persistence.entities.Branch;
import co.aldetec.easyshopbackend.v1.persistence.entities.ErpProduct;
import org.bson.Document;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
@Transactional
public class ErpProductDAO {
    @Autowired
    private SessionFactory sessionFactory;

    public List<ErpProduct> findAll() throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select ep from ErpProduct ep");
            return query.list();
        } catch (Exception ex) {
            Logger.getLogger(ErpProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public ErpProduct findById(Integer id) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select ep from ErpProduct ep where ep.id = :id");
            query.setParameter("id", id);
            return (ErpProduct) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(ErpProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public ErpProduct findBySku(Branch branch, String sku) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select ep from ErpProduct ep where ep.branch = :branch and ep.sku = :sku");
            query.setParameter("branch", branch);
            query.setParameter("sku", sku);
            return (ErpProduct) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(ErpProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public ErpProduct findByEAN(String ean) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select ep from ErpProduct ep where ep.ean = :ean");
            query.setParameter("ean", ean);
            return (ErpProduct) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(ErpProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public List<ErpProduct> findByName(String name) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select ep from ErpProduct ep where ep.name like %:name%");
            query.setParameter("name", name);
            return query.list();
        } catch (Exception ex) {
            Logger.getLogger(ErpProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public List<ErpProduct> findBySearchableName(String searchableName) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select ep from ErpProduct ep where ep.searchableName like %:searchableName%");
            query.setParameter("searchableName", searchableName);
            return query.list();
        } catch (Exception ex) {
            Logger.getLogger(ErpProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public ErpProduct save(ErpProduct erpProduct) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.save(erpProduct);
            Query query = session.createSQLQuery("SELECT currval(pg_get_serial_sequence('erp_product','id'))");
            Integer id = ((BigInteger) query.uniqueResult()).intValue();
            query = session.createQuery("select ep from ErpProduct ep where ep.id = :id");
            query.setParameter("id", id);
            ErpProduct erpProductSaved = (ErpProduct) query.uniqueResult();
            session.getTransaction().commit();
            return erpProductSaved;
        } catch (Exception ex) {
            Logger.getLogger(ErpProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public ErpProduct update(ErpProduct erpProduct) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.update(erpProduct);
            Query query = session.createQuery("select ep from ErpProduct ep where ep.id = :id");
            query.setParameter("id", erpProduct.getId());
            ErpProduct erpProductSaved = (ErpProduct) query.uniqueResult();
            session.getTransaction().commit();
            return erpProductSaved;
        } catch (Exception ex) {
            Logger.getLogger(ErpProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public void updateMany(Branch branch, List<Document> lep) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            Query query = session.createQuery("select count(ep) from ErpProduct ep");
            Integer c = ((Long) query.uniqueResult()).intValue();

            if(c > 0){
                query = session.createQuery("update ErpProduct ep set ep.state = :state");
                query.setParameter("state", false);
                query.executeUpdate();
            }

            for( Document ep : lep ){
                query = session.createQuery("select ep from ErpProduct ep where ep.sku = :sku ");
                query.setParameter("sku", ep.getString("sku"));
                ErpProduct erpProduct = (ErpProduct) query.uniqueResult();
                if(erpProduct == null){
                    erpProduct = new ErpProduct();
                    erpProduct.setSku(ep.getString("sku"));

                }
                erpProduct.setBranch(branch);
                erpProduct.setEan(ep.getString("ean"));
                erpProduct.setName(ep.getString("name"));
                erpProduct.setCategoryCode1(ep.getString("categoryCode1"));
                erpProduct.setCategoryName1(ep.getString("categoryName1"));
                erpProduct.setCategoryCode2(ep.getString("categoryCode2"));
                erpProduct.setCategoryName2(ep.getString("categoryName2"));
                erpProduct.setCategoryCode3(ep.getString("categoryCode3"));
                erpProduct.setCategoryName3(ep.getString("categoryName3"));
                erpProduct.setMark(ep.getString("mark"));
                erpProduct.setSupplier(ep.getString("supplier"));
                erpProduct.setState(ep.getBoolean("state"));

                session.saveOrUpdate(erpProduct);
            }
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(ErpProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public void delete(ErpProduct erpProduct) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.delete(erpProduct);
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(ErpProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }
}
