package co.aldetec.easyshopbackend.v1.persistence.dao;

import co.aldetec.easyshopbackend.v1.persistence.entities.Branch;
import co.aldetec.easyshopbackend.v1.persistence.entities.ErpProduct;
import co.aldetec.easyshopbackend.v1.persistence.entities.ErpProductPrice;
import org.bson.Document;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
@Transactional
public class ErpProductPriceDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public List<ErpProductPrice> findAll() throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select epp from ErpProductPrice epp");
            return query.list();
        } catch (Exception ex) {
            Logger.getLogger(ErpProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public List<ErpProductPrice> findByErpProductId(Integer erpProductId) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select epp from ErpProductPrice epp where epp.erpProduct.id : erpProductId");
            query.setParameter("erpProductId", erpProductId);
            return query.list();
        } catch (Exception ex) {
            Logger.getLogger(ErpProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public ErpProductPrice findByErpId(Integer id) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select epp from ErpProductPrice epp where epp.id :id");
            query.setParameter("id", id);
            return (ErpProductPrice) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(ErpProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public ErpProductPrice save(ErpProductPrice erpProductPrice) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.save(erpProductPrice);
            Query query = session.createSQLQuery("SELECT currval(pg_get_serial_sequence('erp_product_price','id'))");
            Integer id = ((BigInteger) query.uniqueResult()).intValue();
            query = session.createQuery("select epp from ErpProductPrice epp where epp.id = :id");
            query.setParameter("id", id);
            ErpProductPrice erpProductPriceSaved = (ErpProductPrice) query.uniqueResult();
            session.getTransaction().commit();
            return erpProductPriceSaved;
        } catch (Exception ex) {
            Logger.getLogger(ErpProductPriceDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public ErpProductPrice update(ErpProductPrice erpProductPrice) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.update(erpProductPrice);
            Query query = session.createQuery("select epp from ErpProductPrice epp where epp.id = :id");
            query.setParameter("id", erpProductPrice.getId());
            ErpProductPrice erpProductPriceSaved = (ErpProductPrice) query.uniqueResult();
            session.getTransaction().commit();
            return erpProductPriceSaved;
        } catch (Exception ex) {
            Logger.getLogger(ErpProductPriceDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public void updateMany(Branch branch, List<Document> lepp) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            Query query = session.createQuery("delete from ErpProductPrice where branch = :branch ");
            query.setParameter("branch", branch);
            query.executeUpdate();

            String sku = new String();
            for( Document epp : lepp ){

                query = session.createQuery("select ep from ErpProduct ep where ep.branch = :branch and ep.sku = :sku");
                query.setParameter("branch", branch);
                query.setParameter("sku", epp.getString("sku"));
                ErpProduct erpProduct = (ErpProduct) query.uniqueResult();

                if(erpProduct != null){
                    ErpProductPrice erpProductPrice = new ErpProductPrice();
                    erpProductPrice.setErpProduct(erpProduct);
                    erpProductPrice.setUm(epp.getString("um"));
                    erpProductPrice.setUmName(epp.getString("umName"));
                    erpProductPrice.setFactor(epp.getDouble("factor"));
                    erpProductPrice.setMainUM(epp.getBoolean("isMainUm"));
                    erpProductPrice.setStock(epp.getDouble("stock"));
                    erpProductPrice.setPrice(epp.getDouble("price"));
                    erpProductPrice.setDiscountPrice(epp.getDouble("discountPrice"));
                    erpProductPrice.setBranch(branch);
                    session.save(erpProductPrice);
                }
            }
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(ErpProductPriceDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }
}
