package co.aldetec.easyshopbackend.v1.persistence.dao;

import co.aldetec.easyshopbackend.v1.persistence.entities.Branch;
import co.aldetec.easyshopbackend.v1.persistence.entities.Company;
import co.aldetec.easyshopbackend.v1.persistence.entities.Customer;
import co.aldetec.easyshopbackend.v1.persistence.entities.Order;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
@Transactional
public class OrderDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public List<Order> findAll() throws Exception {
        Session session = this.sessionFactory.openSession();
        List<Order> orders = new ArrayList<Order>();
        try{
            Query query = session.createQuery("select order from Order order");
            orders = query.list();
        } catch (Exception ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
        return orders;
    }

    public Order findById(Integer id) throws Exception {
        Session session = this.sessionFactory.openSession();
        Order order = null;
        try{
            Query query = session.createQuery("select order from Order order where order.id = :id");
            query.setParameter("id", id);
            order = (Order) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
        return order;
    }

    public List<Order> findAllByCustomer(Customer customer) throws Exception {
        Session session = this.sessionFactory.openSession();
        List<Order> orders = new ArrayList<Order>();
        try{
            Query query = session.createQuery("select order from Order order where order.customer = :customer");
            query.setParameter("customer", customer);
            orders = query.list();
        } catch (Exception ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
        return orders;
    }

    public List<Order> findAllByBranch(Branch branch) throws Exception {
        Session session = this.sessionFactory.openSession();
        List<Order> orders = new ArrayList<Order>();
        try{
            Query query = session.createQuery("select branch from Order order where order.branch = :branch");
            query.setParameter("branch", branch);
            orders = query.list();
        } catch (Exception ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
        return orders;
    }

    public Order save(Order order) throws Exception {
        Session session = this.sessionFactory.openSession();
        Order newOrder;
        try{
            session.beginTransaction();
            session.save(order);
            Query query = session.createSQLQuery("SELECT currval(pg_get_serial_sequence('order','id'))");
            Integer id = ((BigInteger) query.uniqueResult()).intValue();
            query = session.createQuery("select order from Order order where order.id = :id");
            query.setParameter("id", id);
            newOrder = (Order) query.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(CompanyDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
        return newOrder;
    }

    public Order update(Order order) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.update(order);
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(CompanyDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
        return order;
    }

    public void delete(Order order) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.delete(order);
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(CompanyDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

}
