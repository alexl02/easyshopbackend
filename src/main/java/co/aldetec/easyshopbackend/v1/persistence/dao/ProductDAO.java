package co.aldetec.easyshopbackend.v1.persistence.dao;

import co.aldetec.easyshopbackend.v1.misc.CustomGson;
import co.aldetec.easyshopbackend.v1.persistence.entities.*;
import com.google.gson.Gson;
import org.bson.Document;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
@Transactional
public class ProductDAO {
    @Autowired
    private SessionFactory sessionFactory;

    public List<Product> findAll() throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select ep from Product ep");
            return query.list();
        } catch (Exception ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public Product findById(Company company, Integer id) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select p from Product p where p.id = :id and p.erpProduct.branch.company = :company");
            query.setParameter("id", id);
            query.setParameter("company", company);
            return (Product) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public Document findByIdAndMu(Company company, Integer id, String mu) throws Exception {
        Session session = this.sessionFactory.openSession();
        Gson gson = CustomGson.getCustomGson();
        Document dProduct = new Document();
        try{
            Query query = session.createQuery("select p from Product p where p.id = :id and p.erpProduct.branch.company = :company");
            query.setParameter("id", id);
            query.setParameter("company", company);
            Product product = (Product) query.uniqueResult();

            if(product == null){
                return null;
            }

            query = session.createQuery("select epp from ErpProductPrice epp where epp.erpProduct = :erpProduct and epp.um = :mu");
            query.setParameter("erpProduct", product.getErpProduct());
            query.setParameter("mu", mu);
            ErpProductPrice erpProductPrice = (ErpProductPrice) query.uniqueResult();

            if(erpProductPrice == null) {
                return null;
            }

            dProduct = Document.parse(gson.toJson(product));
            dProduct.append("price", Document.parse(gson.toJson(erpProductPrice)));

        } catch (Exception ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }

        return dProduct;
    }

    public List<Document> findAllByCategory(Company company) throws Exception {
        Gson gson = CustomGson.getCustomGson();
        Session session = this.sessionFactory.openSession();
        List<Document> dCategories = new ArrayList<Document>();
        try{
            Query query = session.createQuery("select c from CategoryLevel1 c where c.company = :company and c.state = :state");
            query.setParameter("company", company);
            query.setParameter("state", true);
            List<CategoryLevel1> categories = query.list();

            for(CategoryLevel1 category : categories ) {

                Document dCategory = Document.parse(gson.toJson(category));
                dCategory.remove("categoryLevel2List");
                dCategory.remove("company");

                query = session.createQuery("select pc from ProductCategory pc where pc.categoryLevel2.categoryLevel1.id = : id and pc.product.erpProduct.state = :state");
                query.setParameter("id", category.getId());
                query.setParameter("state", true);
                query.setMaxResults(10);
                List<ProductCategory> productCategories = query.list();

                List<Document> dProducts = new ArrayList<Document>();
                for ( ProductCategory productCategory : productCategories ) {
                    query = session.createQuery("select epp from ErpProductPrice epp where epp.erpProduct.id = :id and epp.stock > :stock");
                    query.setParameter("id", productCategory.getProduct().getErpProduct().getId());
                    query.setParameter("stock", Double.valueOf(0.0));
                    List<ErpProductPrice> erpProductPrices = query.list();

                    if(erpProductPrices.size() > 0){
                        Document dProduct = Document.parse(gson.toJson(productCategory.getProduct()));
                        dProduct.append("price", Document.parse(gson.toJson(erpProductPrices.get(0))));
                        dProducts.add(dProduct);
                    }

                }

                dCategory.append("products", dProducts);
                if(dProducts.size() > 0) {
                    dCategories.add(dCategory);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }

        return dCategories;
    }

    public Document findByCategory(Company company, Integer categoryId) throws Exception {
        Gson gson = CustomGson.getCustomGson();
        Session session = this.sessionFactory.openSession();
        Document dCategory = new Document();
        try{
            Query query = session.createQuery("select c from CategoryLevel1 c where c.company = :company and c.id = :id and c.state = :state");
            query.setParameter("company", company);
            query.setParameter("id", categoryId);
            query.setParameter("state", true);
            CategoryLevel1 category = (CategoryLevel1) query.uniqueResult();

            if(category == null) {
                return null;
            }

            query = session.createQuery("select pc from ProductCategory pc where pc.categoryLevel2.categoryLevel1.id = : id and pc.product.erpProduct.state = :state");
            query.setParameter("id", category.getId());
            query.setParameter("state", true);
            query.setMaxResults(10);
            List<ProductCategory> productCategories = query.list();

            dCategory = Document.parse(gson.toJson(category));
            dCategory.remove("categoryLevel2List");
            dCategory.remove("company");

            List<Document> dProducts = new ArrayList<Document>();
            for ( ProductCategory productCategory : productCategories ) {
                query = session.createQuery("select epp from ErpProductPrice epp where epp.erpProduct.id = :id and epp.stock > :stock");
                query.setParameter("id", productCategory.getProduct().getErpProduct().getId());
                query.setParameter("stock", Double.valueOf(0.0));
                List<ErpProductPrice> erpProductPrices = query.list();

                for( ErpProductPrice erpProductPrice : erpProductPrices){
                    Document dProduct = Document.parse(gson.toJson(productCategory.getProduct()));
                    dProduct.append("price", Document.parse(gson.toJson(erpProductPrice)));
                    dProducts.add(dProduct);
                }

            }

            dCategory.append("products", dProducts);

        } catch (Exception ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }

        return dCategory;
    }

    public Document findBySubCategory(Company company, Integer subCategoryId) throws Exception {
        Gson gson = CustomGson.getCustomGson();
        Session session = this.sessionFactory.openSession();
        Document dSubCategory = new Document();
        try{
            Query query = session.createQuery("select c2 from CategoryLevel2 c2 where c2.categoryLevel1.company = :company and c2.id = :id and c2.state = :state");
            query.setParameter("company", company);
            query.setParameter("id", subCategoryId);
            query.setParameter("state", true);
            CategoryLevel2 subCategory = (CategoryLevel2) query.uniqueResult();

            if(subCategory == null) {
                return null;
            }

            query = session.createQuery("select pc from ProductCategory pc where pc.categoryLevel2.id = : id and pc.product.erpProduct.state = :state");
            query.setParameter("id", subCategory.getId());
            query.setParameter("state", true);
            query.setMaxResults(10);
            List<ProductCategory> productCategories = query.list();

            dSubCategory = Document.parse(gson.toJson(subCategory));

            List<Document> dProducts = new ArrayList<Document>();
            for ( ProductCategory productCategory : productCategories ) {
                query = session.createQuery("select epp from ErpProductPrice epp where epp.erpProduct.id = :id and epp.stock > :stock");
                query.setParameter("id", productCategory.getProduct().getErpProduct().getId());
                query.setParameter("stock", Double.valueOf(0.0));
                List<ErpProductPrice> erpProductPrices = query.list();

                for( ErpProductPrice erpProductPrice : erpProductPrices){
                    Document dProduct = Document.parse(gson.toJson(productCategory.getProduct()));
                    dProduct.append("price", Document.parse(gson.toJson(erpProductPrice)));
                    dProducts.add(dProduct);
                }

            }

            dSubCategory.append("products", dProducts);

        } catch (Exception ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }

        return dSubCategory;
    }

    public List<Document> search(Company company, String word) throws Exception {
        Gson gson = CustomGson.getCustomGson();
        Session session = this.sessionFactory.openSession();
        List<Document> dProducts = new ArrayList<Document>();
        try{
            Query query = session.createQuery("select p from Product p where p.erpProduct.branch.company = :company and p.searchableWords like :word and p.erpProduct.state = :state");
            query.setParameter("company", company);
            query.setParameter("word", "%" + word + "%");
            query.setParameter("state", true);
            List<Product> products = query.list();

            for ( Product product : products ) {
                query = session.createQuery("select epp from ErpProductPrice epp where epp.erpProduct.id = :id and epp.stock > :stock");
                query.setParameter("id", product.getErpProduct().getId());
                query.setParameter("stock", Double.valueOf(0.0));
                List<ErpProductPrice> erpProductPrices = query.list();

                for( ErpProductPrice erpProductPrice : erpProductPrices){
                    Document dProduct = Document.parse(gson.toJson(product));
                    dProduct.append("price", Document.parse(gson.toJson(erpProductPrice)));
                    dProducts.add(dProduct);
                }

            }

        } catch (Exception ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }

        return dProducts;
    }

    public List<Document> searchAutocomplete(Company company, String word) throws Exception {
        Gson gson = CustomGson.getCustomGson();
        Session session = this.sessionFactory.openSession();
        List<Document> dProducts = new ArrayList<Document>();
        try{
            /*Query query = session.createQuery("select pc from ProductCategory pc where pc.product.erpProduct.branch.company = :company and pc.product.searchableWords like :word and pc.product.erpProduct.state = :state");*/
            Query query = session.createQuery("select p from Product p where p.erpProduct.branch.company = :company and p.searchableWords like :word and p.erpProduct.state = :state");
            query.setParameter("company", company);
            query.setParameter("word", "%" + word + "%");
            query.setParameter("state", true);
            query.setMaxResults(10);
            List<Product> products = query.list();

            for ( Product product : products ) {
                query = session.createQuery("select epp from ErpProductPrice epp where epp.erpProduct.id = :id and epp.stock > :stock");
                query.setParameter("id", product.getErpProduct().getId());
                query.setParameter("stock", Double.valueOf(0.0));
                List<ErpProductPrice> erpProductPrices = query.list();

                if(erpProductPrices.size() > 0) {
                    Document dProduct = Document.parse(gson.toJson(product));
                    dProduct.append("price", Document.parse(gson.toJson(erpProductPrices.get(0))));
                    dProducts.add(dProduct);
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }

        return dProducts;
    }

    public List<Document> findAllById(Company company, Integer id) throws Exception {
        Gson gson = CustomGson.getCustomGson();
        Session session = this.sessionFactory.openSession();
        List<Document> dProducts = new ArrayList<Document>();
        try{

            Query query = session.createQuery("select p from Product p where p.id = :id and p.erpProduct.branch.company = :company and p.erpProduct.state = :state");
            query.setParameter("id", id);
            query.setParameter("company", company);
            query.setParameter("state", true);
            Product product = (Product) query.uniqueResult();

            if(product == null){
                return null;
            }

            query = session.createQuery("select epp from ErpProductPrice epp where epp.erpProduct.id = :id and epp.stock > :stock");
            query.setParameter("id", product.getId());
            query.setParameter("stock", Double.valueOf(0.0));
            List<ErpProductPrice> erpProductPrices = query.list();

            dProducts = new ArrayList<Document>();
            if(erpProductPrices.size() > 0){
                for(ErpProductPrice erpProductPrice : erpProductPrices){
                    Document dProduct = Document.parse(gson.toJson(product));
                    dProduct.append("price", Document.parse(gson.toJson(erpProductPrice)));
                    dProducts.add(dProduct);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }

        return dProducts;
    }

    public Product save(Product product) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.save(product);
            Query query = session.createSQLQuery("SELECT currval(pg_get_serial_sequence('erp_product','id'))");
            Integer id = ((BigInteger) query.uniqueResult()).intValue();
            query = session.createQuery("select ep from Product ep where ep.id = :id");
            query.setParameter("id", id);
            Product productSaved = (Product) query.uniqueResult();
            session.getTransaction().commit();
            return productSaved;
        } catch (Exception ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public void saveMany(List<Product> lep) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            for( Product product : lep ){
                session.save(product);
            }
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public Product update(Product product) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.update(product);
            Query query = session.createQuery("select ep from Product ep where ep.id = :id");
            query.setParameter("id", product.getId());
            Product productSaved = (Product) query.uniqueResult();
            session.getTransaction().commit();
            return productSaved;
        } catch (Exception ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public void updateMany(Company company, List<String> data) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            Query query = session.createQuery("select b from Branch b where b.company = : company");
            query.setParameter("company", company);
            List<Branch> branches = query.list();

            for( Branch branch : branches ){
                for( String line : data ){
                    String fields[] = line.split(";");

                    query = session.createQuery("select ep from ErpProduct ep where ep.branch = :branch and ep.sku = :sku");
                    query.setParameter("branch", branch);
                    query.setParameter("sku", fields[0]);
                    ErpProduct erpProduct = (ErpProduct) query.uniqueResult();
                    if (erpProduct == null) {
                        continue;
                    }

                    Product product = new Product();
                    product.setId(erpProduct.getId());
                    product.setName(fields[2]);
                    product.setPumUm(fields[3]);
                    product.setPumContent(Double.valueOf(fields[4]));
                    product.setSearchableWords(fields[5]);
                    product.setImgUrl(erpProduct.getSku() + ".jpg");

                    session.saveOrUpdate(product);

                    query = session.createQuery("delete from ProductCategory where product = :product");
                    query.setParameter("product", product);
                    query.executeUpdate();

                    String categories[] = fields[6].split(",");
                    for( String categoryId : categories ){
                        ProductCategory productCategory = new ProductCategory();
                        query = session.createQuery("select c2 from CategoryLevel2 c2 where c2.categoryLevel1.company = :company and c2.alterCode = :alterCode");
                        query.setParameter("company", company);
                        query.setParameter("alterCode", categoryId);
                        CategoryLevel2 categoryLevel2 = (CategoryLevel2) query.uniqueResult();

                        if(categoryLevel2 == null){
                            continue;
                        }

                        productCategory.setCategoryLevel2(categoryLevel2);
                        productCategory.setProduct(product);
                        session.saveOrUpdate(productCategory);
                    }
                }
            }
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex);
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public void updateManyByBranch(Branch branch, List<Document> products) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();

            for( Document dProduct : products ){
                String pum[] = dProduct.getString("pum").split(",");
                String categories[] = dProduct.getString("cat").split(",");

                Query query = session.createQuery("select ep from ErpProduct ep where ep.branch = :branch and ep.sku = :sku");
                query.setParameter("branch", branch);
                query.setParameter("sku", dProduct.getString("sku"));
                ErpProduct erpProduct = (ErpProduct) query.uniqueResult();
                if (erpProduct == null) {
                    continue;
                }

                query = session.createQuery("select p from Product p where p.id = :id");
                query.setParameter("id", erpProduct.getId());
                Product product = (Product) query.uniqueResult();

                if(product == null){
                    product = new Product();
                    product.setId(erpProduct.getId());
                    product.setImgUrl(erpProduct.getSku() + ".jpg");
                }

                product.setPumUm(pum[0]);
                product.setPumContent(Double.valueOf(pum[1]));

                session.saveOrUpdate(product);

                query = session.createQuery("delete from ProductCategory where product = :product");
                query.setParameter("product", product);
                query.executeUpdate();

                for( String categoryId : categories ){
                    ProductCategory productCategory = new ProductCategory();
                    query = session.createQuery("select c2 from CategoryLevel2 c2 where c2.categoryLevel1.company = :company and c2.alterCode = :alterCode");
                    query.setParameter("company", branch.getCompany());
                    query.setParameter("alterCode", categoryId);
                    CategoryLevel2 categoryLevel2 = (CategoryLevel2) query.uniqueResult();

                    if(categoryLevel2 == null){
                        continue;
                    }

                    productCategory.setCategoryLevel2(categoryLevel2);
                    productCategory.setProduct(product);
                    session.saveOrUpdate(productCategory);
                }
            }
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex);
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public void delete(Product product) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.delete(product);
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }
}
