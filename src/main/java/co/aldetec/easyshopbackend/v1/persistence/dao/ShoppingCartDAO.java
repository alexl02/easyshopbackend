package co.aldetec.easyshopbackend.v1.persistence.dao;

import co.aldetec.easyshopbackend.v1.persistence.entities.Product;
import co.aldetec.easyshopbackend.v1.persistence.entities.ShoppingCart;
import co.aldetec.easyshopbackend.v1.persistence.entities.ShoppingCartDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
@Transactional
public class ShoppingCartDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public ShoppingCart findBySessionId(String sessionId) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            Query query = session.createQuery("select sc from ShopingCart where sc.sessionId = :sessionId");
            query.setParameter("sessionId", sessionId);
            return (ShoppingCart) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(ShoppingCartDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public ShoppingCart save(ShoppingCart shoppingCart) throws Exception {
        Session session = this.sessionFactory.openSession();
        ShoppingCart shoppingCartNew = null;
        try{
            session.beginTransaction();
            session.save(shoppingCart);
            Query query = session.createSQLQuery("SELECT currval(pg_get_serial_sequence('shopping_cart','id'))");
            Integer id = ((BigInteger) query.uniqueResult()).intValue();
            query = session.createQuery("select shoppingCart from ShoppingCart shoppingCart where shoppingCart.id = :id");
            query.setParameter("id", id);
            shoppingCartNew = (ShoppingCart) query.uniqueResult();
            session.getTransaction().commit();

        } catch (Exception ex) {
            Logger.getLogger(ShoppingCartDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
        return shoppingCartNew;
    }

    public ShoppingCart update(ShoppingCart shoppingCart) throws Exception {
        Session session = this.sessionFactory.openSession();
        ShoppingCart shoppingCartNew = null;
        try{
            session.beginTransaction();
            session.update(shoppingCart);
            Query query = session.createQuery("select shoppingCart from ShoppingCart shoppingCart where shoppingCart.id = :id");
            query.setParameter("id", shoppingCart.getId());
            shoppingCartNew = (ShoppingCart) query.uniqueResult();
            session.getTransaction().commit();

        } catch (Exception ex) {
            Logger.getLogger(ShoppingCartDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
        return shoppingCartNew;
    }

    public void delete(ShoppingCart shoppingCart) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.delete(shoppingCart);
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(ShoppingCartDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public ShoppingCartDetail findById(Integer id) throws Exception {
        Session session = this.sessionFactory.openSession();
        ShoppingCartDetail shoppingCartDetail = null;
        try{
            Query query = session.createQuery("select shoppingCartDetail from ShoppingCartDetail shoppingCartDetail where shoppingCartDetail.id = :id");
            query.setParameter("id", id);
            shoppingCartDetail = (ShoppingCartDetail) query.uniqueResult();
        } catch (Exception ex) {
            Logger.getLogger(ShoppingCartDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
        return shoppingCartDetail;
    }

    public List<ShoppingCartDetail> findShoppingCartDetailsBySessionId(Session sessionId) throws Exception {
        Session session = this.sessionFactory.openSession();
        List<ShoppingCartDetail> shoppingCartDetails = null;
        try{
            Query query = session.createQuery("select shoppingCartDetail from ShoppingCartDetail shoppingCartDetail where shoppingCartDetail.shoppingCart.sessionId = :sessionId");
            query.setParameter("sessionId", sessionId);
            shoppingCartDetails = query.list();
        } catch (Exception ex) {
            Logger.getLogger(ShoppingCartDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }

        return shoppingCartDetails;
    }

    public ShoppingCartDetail saveShoppingCartDetail(ShoppingCartDetail shoppingCartDetail) throws Exception {
        Session session = this.sessionFactory.openSession();
        ShoppingCartDetail shoppingCartDetailNew = null;
        try{
            session.beginTransaction();
            session.save(shoppingCartDetail);
            Query query = session.createSQLQuery("SELECT currval(pg_get_serial_sequence('shopping_cart_detail','id'))");
            Integer id = ((BigInteger) query.uniqueResult()).intValue();
            query = session.createQuery("select shoppingCartDetail from ShoppingCartDetail shoppingCartDetail where shoppingCartDetail.id = :id");
            query.setParameter("id", id);
            shoppingCartDetailNew = (ShoppingCartDetail) query.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(ShoppingCartDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
        return shoppingCartDetailNew;
    }

    public ShoppingCartDetail updateShoppingCartDetail(ShoppingCartDetail shoppingCartDetail) throws Exception {
        Session session = this.sessionFactory.openSession();
        ShoppingCartDetail shoppingCartDetailNew = null;
        try{
            session.beginTransaction();
            session.update(shoppingCartDetail);
            Query query = session.createQuery("select shoppingCartDetail from ShoppingCartDetail shoppingCartDetail where shoppingCartDetail.id = :id");
            query.setParameter("id", shoppingCartDetail.getId());
            shoppingCartDetailNew = (ShoppingCartDetail) query.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(ShoppingCartDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
        return shoppingCartDetailNew;
    }

    public void deleteShoppingCartDetail(ShoppingCartDetail shoppingCartDetail) throws Exception {
        Session session = this.sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.delete(shoppingCartDetail);
            session.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(ShoppingCartDAO.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
            throw new Exception(ex.getCause());
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }
}
