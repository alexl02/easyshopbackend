package co.aldetec.easyshopbackend.v1.persistence.entities;

import javax.persistence.*;

@Entity
@Table( name = "public.branch", uniqueConstraints = {@UniqueConstraint(columnNames = {"company_id", "erp_branch_id"})})
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id") private Integer id;
    @Column(name = "erp_branch_id") private String erpBranchId;
    @Column(name = "name") private String name;
    @Column(name = "state") private Boolean state;
    @ManyToOne @JoinColumn(name="company_id") Company company;

    public Branch() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getErpBranchId() {
        return erpBranchId;
    }

    public void setErpBranchId(String erpBranchId) {
        this.erpBranchId = erpBranchId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
