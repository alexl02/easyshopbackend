package co.aldetec.easyshopbackend.v1.persistence.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table( name = "public.category_level_1", uniqueConstraints = {@UniqueConstraint(name = "unq_category_level_1_company_id_alter_code", columnNames = {"company_id", "alter_code"})})
public class CategoryLevel1 implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id") private Integer id;
    @Column(name = "alter_code") private String alterCode;
    @Column(name = "name") private String name;
    @Column(name = "searchable_words") private String searchableWords;
    @Column(name = "img_url") private String imgUrl;
    @Column(name = "state") private Boolean state;
    @ManyToOne @JoinColumn(name="company_id") Company company;
    @OneToMany(cascade= CascadeType.ALL, fetch = FetchType.EAGER) @JoinColumn(name="category_level_1_id")  List<CategoryLevel2> categoryLevel2List;

    public CategoryLevel1() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAlterCode() {
        return alterCode;
    }

    public void setAlterCode(String alterCode) {
        this.alterCode = alterCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSearchableWords() {
        return searchableWords;
    }

    public void setSearchableWords(String searchableWords) {
        this.searchableWords = searchableWords;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<CategoryLevel2> getCategoryLevel2List() {
        return categoryLevel2List;
    }

    public void setCategoryLevel2List(List<CategoryLevel2> categoryLevel2List) {
        this.categoryLevel2List = categoryLevel2List;
    }
}
