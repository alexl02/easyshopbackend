package co.aldetec.easyshopbackend.v1.persistence.entities;

import co.aldetec.easyshopbackend.v1.annotations.JsonExclude;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table( name = "public.category_level_2", uniqueConstraints = {@UniqueConstraint(name = "unq_category_level_2_category_level_1_id_id_alter_code", columnNames = {"category_level_1_id", "alter_code"})})
public class CategoryLevel2 implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id") private Integer id;
    @Column(name = "alter_code") private String alterCode;
    @Column(name = "name") private String name;
    @Column(name = "searchable_words") private String searchableWords;
    @Column(name = "state") private Boolean state;
    @JsonExclude
    @ManyToOne @JoinColumn(name="category_level_1_id") CategoryLevel1 categoryLevel1;

    public CategoryLevel2() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAlterCode() {
        return alterCode;
    }

    public void setAlterCode(String alterCode) {
        this.alterCode = alterCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSearchableWords() {
        return searchableWords;
    }

    public void setSearchableWords(String searchableWords) {
        this.searchableWords = searchableWords;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public CategoryLevel1 getCategoryLevel1() {
        return categoryLevel1;
    }

    public void setCategoryLevel1(CategoryLevel1 categoryLevel1) {
        this.categoryLevel1 = categoryLevel1;
    }
}
