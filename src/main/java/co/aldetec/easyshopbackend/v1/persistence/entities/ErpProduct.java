package co.aldetec.easyshopbackend.v1.persistence.entities;

import co.aldetec.easyshopbackend.v1.annotations.JsonExclude;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table( name = "public.erp_product", uniqueConstraints = {@UniqueConstraint(name = "erp_product_branch_id_sku", columnNames = {"branch_id", "sku"})})
public class ErpProduct implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id") private Integer id;
    @Column(name = "sku", unique = true) private String sku;
    @Column(name = "ean") private String ean;
    @Column(name = "name") private String name;
    @Column(name = "erp_category_code_1") private String categoryCode1;
    @Column(name = "erp_category_name_1") private String categoryName1;
    @Column(name = "erp_category_code_2") private String categoryCode2;
    @Column(name = "erp_category_name_2") private String categoryName2;
    @Column(name = "erp_category_code_3") private String categoryCode3;
    @Column(name = "erp_category_name_3") private String categoryName3;
    @Column(name = "mark") private String mark;
    @Column(name = "supplier") private String supplier;
    @Column(name = "state") private Boolean state;
    @JsonExclude
    @ManyToOne @JoinColumn(name="branch_id") Branch branch;
    public ErpProduct() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryCode1() {
        return categoryCode1;
    }

    public void setCategoryCode1(String categoryCode1) {
        this.categoryCode1 = categoryCode1;
    }

    public String getCategoryName1() {
        return categoryName1;
    }

    public void setCategoryName1(String categoryName1) {
        this.categoryName1 = categoryName1;
    }

    public String getCategoryCode2() {
        return categoryCode2;
    }

    public void setCategoryCode2(String categoryCode2) {
        this.categoryCode2 = categoryCode2;
    }

    public String getCategoryName2() {
        return categoryName2;
    }

    public void setCategoryName2(String categoryName2) {
        this.categoryName2 = categoryName2;
    }

    public String getCategoryCode3() {
        return categoryCode3;
    }

    public void setCategoryCode3(String categoryCode3) {
        this.categoryCode3 = categoryCode3;
    }

    public String getCategoryName3() {
        return categoryName3;
    }

    public void setCategoryName3(String categoryName3) {
        this.categoryName3 = categoryName3;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }
}
