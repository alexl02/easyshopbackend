package co.aldetec.easyshopbackend.v1.persistence.entities;

import co.aldetec.easyshopbackend.v1.annotations.JsonExclude;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table( name = "public.erp_product_price")
public class ErpProductPrice implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id") private Integer id;
    @Column(name = "um") private String um;
    @Column(name = "umName") private String umName;
    @Column(name = "factor") private Double factor;
    @Column(name = "stock") private Double stock;
    @Column(name = "price") private Double price;
    @Column(name = "discount_price") private Double discountPrice;
    @Column(name = "is_main_um") private Boolean isMainUM;
    @JsonExclude
    @ManyToOne @JoinColumn(name="erp_product_id") ErpProduct erpProduct;
    @JsonExclude
    @ManyToOne @JoinColumn(name="branch_id") Branch branch;

    public ErpProductPrice() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    public String getUmName() {
        return umName;
    }

    public void setUmName(String umName) {
        this.umName = umName;
    }

    public Double getFactor() {
        return factor;
    }

    public void setFactor(Double factor) {
        this.factor = factor;
    }

    public Double getStock() {
        return stock;
    }

    public void setStock(Double stock) {
        this.stock = stock;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(Double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public Boolean getMainUM() {
        return isMainUM;
    }

    public void setMainUM(Boolean mainUM) {
        isMainUM = mainUM;
    }

    public ErpProduct getErpProduct() {
        return erpProduct;
    }

    public void setErpProduct(ErpProduct erpProduct) {
        this.erpProduct = erpProduct;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }
}
