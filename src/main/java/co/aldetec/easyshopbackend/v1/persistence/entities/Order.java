package co.aldetec.easyshopbackend.v1.persistence.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "public.order"/*, indexes = {
        @Index(name = "idx_product_searchable_name", columnList = "searchable_name"),
        @Index(name = "idx_product_name", columnList = "name")
}*/)
public class Order implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id") private Integer id;
    @Column(name = "create_at") private Timestamp createAt;
    @Column(name = "address") private String address;
    @Column(name = "address_details") private String addressDetails;
    @Column(name = "telphone_number") private String telephoneNumber;
    @Column(name = "zone") private String zone;
    @OneToMany(cascade= CascadeType.ALL, fetch = FetchType.EAGER) @JoinColumn(name="order_id")
    List<OrderDetail> orderDetails;
    @ManyToOne @JoinColumn(name="customer_id") Customer customer;
    @ManyToOne @JoinColumn(name="branch_id") Branch branch;

    public Order() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Timestamp createAt) {
        this.createAt = createAt;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressDetails() {
        return addressDetails;
    }

    public void setAddressDetails(String addressDetails) {
        this.addressDetails = addressDetails;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }
}
