package co.aldetec.easyshopbackend.v1.persistence.entities;

import co.aldetec.easyshopbackend.v1.annotations.JsonExclude;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "public.order_detail"/*, indexes = {
        @Index(name = "idx_product_searchable_name", columnList = "searchable_name"),
        @Index(name = "idx_product_name", columnList = "name")
}*/)
public class OrderDetail implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id") private Integer id;
    @Column(name = "price") private Double price;
    @Column(name = "discount_price") private Double discountPrice;
    @Column(name = "qty") private Double qty;
    @Column(name = "mu") private String mu;
    @JsonExclude
    @ManyToOne @JoinColumn(name="order_id") Order order;
    @ManyToOne @JoinColumn(name="product_id") Product product;
    public OrderDetail() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(Double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public String getMu() {
        return mu;
    }

    public void setMu(String mu) {
        this.mu = mu;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
