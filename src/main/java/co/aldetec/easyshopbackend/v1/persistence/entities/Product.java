package co.aldetec.easyshopbackend.v1.persistence.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "public.product"/*, indexes = {
        @Index(name = "idx_product_searchable_name", columnList = "searchable_name"),
        @Index(name = "idx_product_name", columnList = "name")
}*/)
public class Product implements Serializable {
    @Id
    @Column(name = "id") private Integer id;
    @Column(name = "name") private String name;
    @Column(name = "searchable_words") private String searchableWords;
    @Column(name = "img_url") private String imgUrl;
    @Column(name = "pum_um") private String pumUm;
    @Column(name = "pum_content") private Double pumContent;
    @OneToOne @PrimaryKeyJoinColumn ErpProduct erpProduct;
    public Product() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSearchableWords() {
        return searchableWords;
    }

    public void setSearchableWords(String searchableWords) {
        this.searchableWords = searchableWords;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getPumUm() {
        return pumUm;
    }

    public void setPumUm(String pumUm) {
        this.pumUm = pumUm;
    }

    public Double getPumContent() {
        return pumContent;
    }

    public void setPumContent(Double pumContent) {
        this.pumContent = pumContent;
    }

    public ErpProduct getErpProduct() {
        return erpProduct;
    }

    public void setErpProduct(ErpProduct erpProduct) {
        this.erpProduct = erpProduct;
    }
}
