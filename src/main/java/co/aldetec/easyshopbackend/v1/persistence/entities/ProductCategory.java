package co.aldetec.easyshopbackend.v1.persistence.entities;

import javax.persistence.*;

@Entity
@Table( name = "public.product_category")
public class ProductCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id") private Integer id;
    @ManyToOne @JoinColumn(name="product_id") Product product;
    @ManyToOne @JoinColumn(name="category_level_2_id") CategoryLevel2 categoryLevel2;

    public ProductCategory() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public CategoryLevel2 getCategoryLevel2() {
        return categoryLevel2;
    }

    public void setCategoryLevel2(CategoryLevel2 categoryLevel2) {
        this.categoryLevel2 = categoryLevel2;
    }
}
