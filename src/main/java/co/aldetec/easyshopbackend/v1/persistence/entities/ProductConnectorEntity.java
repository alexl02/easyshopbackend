package co.aldetec.easyshopbackend.v1.persistence.entities;

public class ProductConnectorEntity {
    private Branch branch;
    private Product product;

    public ProductConnectorEntity() {
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
