package co.aldetec.easyshopbackend.v1.persistence.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "public.shopping_cart"/*, indexes = {
        @Index(name = "idx_product_searchable_name", columnList = "searchable_name"),
        @Index(name = "idx_product_name", columnList = "name")
}*/)
public class ShoppingCart implements Serializable {
    @Id
    @Column(name = "id") private Integer id;
    @Column(name = "session_id") private String sessionid;
    @ManyToOne
    @JoinColumn(name="branch_id") Branch branch;
    @OneToMany(cascade= CascadeType.ALL, fetch = FetchType.EAGER) @JoinColumn(name="shopping_cart_id") List<ShoppingCartDetail> shoppingCartDetails;

    public ShoppingCart() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public List<ShoppingCartDetail> getShoppingCartDetails() {
        return shoppingCartDetails;
    }

    public void setShoppingCartDetails(List<ShoppingCartDetail> shoppingCartDetails) {
        this.shoppingCartDetails = shoppingCartDetails;
    }
}
