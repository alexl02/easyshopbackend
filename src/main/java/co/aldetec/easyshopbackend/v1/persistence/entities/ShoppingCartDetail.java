package co.aldetec.easyshopbackend.v1.persistence.entities;

import co.aldetec.easyshopbackend.v1.annotations.JsonExclude;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "public.shoping_cart_detail"/*, indexes = {
        @Index(name = "idx_product_searchable_name", columnList = "searchable_name"),
        @Index(name = "idx_product_name", columnList = "name")
}*/)
public class ShoppingCartDetail implements Serializable {
    @Id
    @Column(name = "id") private Integer id;
    @Column(name = "qty") private Double qty;
    @ManyToOne
    @JsonExclude
    @JoinColumn(name="shoping_cart_id")
    ShoppingCart shoppingCart;
    @ManyToOne
    @JoinColumn(name="product_id") Product product;
    @ManyToOne
    @JoinColumn(name="erp_product_price_id") ErpProductPrice erpProductPrice;

    public ShoppingCartDetail() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ErpProductPrice getErpProductPrice() {
        return erpProductPrice;
    }

    public void setErpProductPrice(ErpProductPrice erpProductPrice) {
        this.erpProductPrice = erpProductPrice;
    }
}
