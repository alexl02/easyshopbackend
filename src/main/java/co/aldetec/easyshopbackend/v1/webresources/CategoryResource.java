package co.aldetec.easyshopbackend.v1.webresources;

import co.aldetec.easyshopbackend.v1.connectors.CategoryLevel1Connector;
import co.aldetec.easyshopbackend.v1.connectors.CategoryLevel2Connector;
import co.aldetec.easyshopbackend.v1.misc.CustomGson;
import co.aldetec.easyshopbackend.v1.misc.HttpResponses;
import co.aldetec.easyshopbackend.v1.persistence.dao.CategoryLevel1DAO;
import co.aldetec.easyshopbackend.v1.persistence.dao.CategoryLevel2DAO;
import co.aldetec.easyshopbackend.v1.persistence.dao.CompanyDAO;
import co.aldetec.easyshopbackend.v1.persistence.entities.CategoryLevel1;
import co.aldetec.easyshopbackend.v1.persistence.entities.CategoryLevel2;
import co.aldetec.easyshopbackend.v1.persistence.entities.Company;
import com.google.gson.Gson;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/v1/category")
public class CategoryResource {
    @Autowired
    private CompanyDAO companyDAO;

    @Autowired
    private CategoryLevel1DAO categoryLevel1DAO;

    @Autowired
    private CategoryLevel2DAO categoryLevel2DAO;

    @Autowired
    private CategoryLevel1Connector categoryLevel1Connector;

    @Autowired
    private CategoryLevel2Connector categoryLevel2Connector;

    @GET
    @Produces("application/json")
    @Path("/l1")
    public Response getAll(@HeaderParam("api_key") String apiKey){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            Gson gson = CustomGson.getCustomGson();
            return  Response.ok(gson.toJson(categoryLevel1DAO.findAll(company)), MediaType.APPLICATION_JSON).build();

        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/l1/id/{id}")
    public Response getAll(@HeaderParam("api_key") String apiKey, @PathParam("id") String id){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            if(id == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "ERROR_PARAMETER_MISSING", "Error the id parameter is missing").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();
            return  Response.ok(gson.toJson(categoryLevel1DAO.findById(company, Integer.valueOf(id))), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/l1/alter_code/{alterCode}")
    public Response getByAlterCode(@HeaderParam("api_key") String apiKey, @PathParam("alterCode") String alterCode){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            if(alterCode == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "ERROR_PARAMETER_MISSING", "Error the alterCode parameter is missing").toJson()).build();
            }
            Gson gson = CustomGson.getCustomGson();
            return  Response.ok(gson.toJson(categoryLevel1DAO.findByAlterCode(company, alterCode)), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/l1/search/{words}")
    public Response getBySearhableWords(@HeaderParam("api_key") String apiKey, @PathParam("words") String words){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            if(words == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "ERROR_PARAMETER_MISSING", "Error the words parameter is missing").toJson()).build();
            }
            Gson gson = CustomGson.getCustomGson();
            return  Response.ok(gson.toJson(categoryLevel1DAO.findBySearchableWords(company, words)), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @PUT
    @Produces("application/json")
    @Path("/l1")
    public Response save(@HeaderParam("api_key") String apiKey, String content){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();
            Document dCategoryLevel1 = Document.parse(content);

            Response r = validateParams("CATEGORY1", "SAVE", dCategoryLevel1);
            if(r != null){
                return r;
            }

            CategoryLevel1 categoryLevel1 = new CategoryLevel1();
            categoryLevel1.setCompany(company);
            categoryLevel1.setAlterCode(dCategoryLevel1.getString("alterCode"));
            categoryLevel1.setName(dCategoryLevel1.getString("name"));
            categoryLevel1.setSearchableWords(dCategoryLevel1.getString("searchableWords"));
            categoryLevel1.setImgUrl(dCategoryLevel1.getString("imgUrl"));
            categoryLevel1.setState(dCategoryLevel1.getBoolean("state"));

            CategoryLevel1 savedCategoryLevel1 = categoryLevel1DAO.save(categoryLevel1);
            return  Response.ok(gson.toJson(savedCategoryLevel1), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @POST
    @Produces("application/json")
    @Path("/l1")
    public Response update(@HeaderParam("api_key") String apiKey, String content){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();
            Document dCategoryLevel1 = Document.parse(content);

            Response r = validateParams("CATEGORY1", "UPDATE", dCategoryLevel1);
            if(r != null){
                return r;
            }

            CategoryLevel1 categoryLevel1 = categoryLevel1DAO.findById(company, dCategoryLevel1.getInteger("id"));
            if(categoryLevel1 == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "ERROR_DATA_NOT_FOUND", "Error: category not found").toJson()).build();
            }
            categoryLevel1.setName(dCategoryLevel1.getString("name"));
            categoryLevel1.setSearchableWords(dCategoryLevel1.getString("searchableWords"));
            categoryLevel1.setImgUrl(dCategoryLevel1.getString("imgUrl"));
            categoryLevel1.setState(dCategoryLevel1.getBoolean("state"));

            CategoryLevel1 savedCategoryLevel1 = categoryLevel1DAO.update(categoryLevel1);
            return  Response.ok(gson.toJson(savedCategoryLevel1), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @DELETE
    @Produces("application/json")
    @Path("/l1/delete/{id}")
    public Response delete(@HeaderParam("api_key") String apiKey, @PathParam("id") String id){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();
            CategoryLevel1 categoryLevel1 = categoryLevel1DAO.findById(company, Integer.valueOf(id));
            if(categoryLevel1 == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "ERROR_DATA_NOT_FOUND", "Error: category not found").toJson()).build();
            }
            categoryLevel1DAO.delete(categoryLevel1);
            return  Response.ok(HttpResponses.getResponse(200, "SUCCESS", "Category deleted").toJson(), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/l1/connector")
    public Response loadCategoriesL1ByConnector(@HeaderParam("api_key") String apiKey){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            categoryLevel1Connector.loadConnector(company);
            //Gson gson = CustomGson.getCustomGson();
            //return  Response.ok(gson.toJson(categoryLevel1DAO.findAll(company)), MediaType.APPLICATION_JSON).build();
            return Response.status(Response.Status.OK).entity(HttpResponses.getResponse(200, "SUCCESS", "categories loaded succesfully").toJson()).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/l2")
    public Response getAllCategory2(@HeaderParam("api_key") String apiKey){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            Gson gson = CustomGson.getCustomGson();
            return  Response.ok(gson.toJson(categoryLevel2DAO.findAll(company)), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/l2/id/{id}")
    public Response getById(@HeaderParam("api_key") String apiKey, @PathParam("id") String id){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            if(id == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "ERROR_PARAMETER_MISSING", "Error the id parameter is missing").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();
            return  Response.ok(gson.toJson(categoryLevel2DAO.findById(company, Integer.valueOf(id))), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/l2/alter_code/{alterCode}")
    public Response getCategoryLevel2ByAlterCode(@HeaderParam("api_key") String apiKey, @PathParam("alterCode") String alterCode){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            if(alterCode == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "ERROR_PARAMETER_MISSING", "Error the alterCode parameter is missing").toJson()).build();
            }
            Gson gson = CustomGson.getCustomGson();
            return  Response.ok(gson.toJson(categoryLevel2DAO.findByAlterCode(company, alterCode)), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/l2/search/{words}")
    public Response getCategoryLevel2BySearhableWords(@HeaderParam("api_key") String apiKey, @PathParam("words") String words){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            if(words == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "ERROR_PARAMETER_MISSING", "Error the words parameter is missing").toJson()).build();
            }
            Gson gson = CustomGson.getCustomGson();
            return  Response.ok(gson.toJson(categoryLevel2DAO.findBySearchableWords(company, words)), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @PUT
    @Produces("application/json")
    @Path("/l2")
    public Response saveCategory2(@HeaderParam("api_key") String apiKey, String content){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();
            Document dCategoryLevel2 = Document.parse(content);

            Response r = validateParams("CATEGORY2", "SAVE", dCategoryLevel2);
            if(r != null){
                return r;
            }

            CategoryLevel1 categoryLevel1 = categoryLevel1DAO.findById(company, dCategoryLevel2.getInteger("category1Id"));
            if(categoryLevel1 == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "ERROR_DATA_NOT_FOUND", "Error: category not found").toJson()).build();
            }

            CategoryLevel2 categoryLevel2 = new CategoryLevel2();
            categoryLevel2.setCategoryLevel1(categoryLevel1);
            categoryLevel2.setAlterCode(dCategoryLevel2.getString("alterCode"));
            categoryLevel2.setName(dCategoryLevel2.getString("name"));
            categoryLevel2.setSearchableWords(dCategoryLevel2.getString("searchableWords"));
            categoryLevel2.setState(dCategoryLevel2.getBoolean("state"));
            CategoryLevel2 savedCategoryLevel2 = categoryLevel2DAO.save(categoryLevel2);
            return  Response.ok(gson.toJson(savedCategoryLevel2), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @POST
    @Produces("application/json")
    @Path("/l2")
    public Response updateCategoryLevel2(@HeaderParam("api_key") String apiKey, String content){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();
            Document dCategoryLevel2 = Document.parse(content);

            Response r = validateParams("CATEGORY2", "UPDATE", dCategoryLevel2);
            if(r != null){
                return r;
            }

            CategoryLevel2 categoryLevel2 = categoryLevel2DAO.findById(company, dCategoryLevel2.getInteger("id"));
            categoryLevel2.setName(dCategoryLevel2.getString("name"));
            categoryLevel2.setSearchableWords(dCategoryLevel2.getString("searchableWords"));
            categoryLevel2.setState(dCategoryLevel2.getBoolean("state"));

            CategoryLevel2 savedCategoryLevel2 = categoryLevel2DAO.update(categoryLevel2);
            return  Response.ok(gson.toJson(savedCategoryLevel2), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @DELETE
    @Produces("application/json")
    @Path("/l2/delete/{id}")
    public Response deleteCategoryLevel2(@HeaderParam("api_key") String apiKey, @PathParam("id") String id){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();
            CategoryLevel2 categoryLevel2 = categoryLevel2DAO.findById(company, Integer.valueOf(id));
            if(categoryLevel2 == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "ERROR_DATA_NOT_FOUND", "Error: category not found").toJson()).build();
            }
            categoryLevel2DAO.delete(categoryLevel2);
            return  Response.ok(HttpResponses.getResponse(200, "SUCCESS", "Category deleted").toJson(), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/l2/connector")
    public Response loadCategoriesL2ByConnector(@HeaderParam("api_key") String apiKey){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            categoryLevel2Connector.loadConnector(company);
            //Gson gson = CustomGson.getCustomGson();
            //return  Response.ok(gson.toJson(categoryLevel1DAO.findAll(company)), MediaType.APPLICATION_JSON).build();
            return Response.status(Response.Status.OK).entity(HttpResponses.getResponse(200, "SUCCESS", "categories loaded succesfully").toJson()).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    private Response validateParams(String entity, String action, Document d){
        if(entity.equals("CATEGORY1")){
            if((action.equals("UPDATE")) && (d.get("id") == null)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: id parameter is missing").toJson()).build();
            }

            if((action.equals("SAVE") || (action.equals("UPDATE"))) && (d.get("alterCode") == null)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: alterCode parameter is missing").toJson()).build();
            }

            if((action.equals("SAVE") || (action.equals("UPDATE"))) && (d.get("name") == null)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: name parameter is missing").toJson()).build();
            }

            if((action.equals("SAVE") || (action.equals("UPDATE"))) && (d.get("searchableWords") == null)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: searchableWords parameter is missing").toJson()).build();
            }

            if((action.equals("SAVE") || (action.equals("UPDATE"))) && (d.get("imgUrl") == null)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: imgUrl parameter is missing").toJson()).build();
            }

            if((action.equals("SAVE") || (action.equals("UPDATE"))) && (d.get("state") == null)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: state parameter is missing").toJson()).build();
            }
        } else if(entity.equals("CATEGORY2")){
            if((action.equals("UPDATE")) && (d.get("id") == null)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: id parameter is missing").toJson()).build();
            }

            if((action.equals("SAVE") || (action.equals("UPDATE"))) && (d.get("alterCode") == null)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: alterCode parameter is missing").toJson()).build();
            }

            if((action.equals("SAVE") || (action.equals("UPDATE"))) && (d.get("name") == null)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: name parameter is missing").toJson()).build();
            }

            if((action.equals("SAVE") || (action.equals("UPDATE"))) && (d.get("searchableWords") == null)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: searchableWords parameter is missing").toJson()).build();
            }

            if((action.equals("SAVE") || (action.equals("UPDATE"))) && (d.get("state") == null)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: state parameter is missing").toJson()).build();
            }
        }

        return null;
    }
}
