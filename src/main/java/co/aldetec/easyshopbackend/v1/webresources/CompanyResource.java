package co.aldetec.easyshopbackend.v1.webresources;

import co.aldetec.easyshopbackend.v1.misc.Crypt;
import co.aldetec.easyshopbackend.v1.misc.DateUtils;
import co.aldetec.easyshopbackend.v1.misc.HttpResponses;
import co.aldetec.easyshopbackend.v1.persistence.dao.CompanyDAO;
import co.aldetec.easyshopbackend.v1.persistence.entities.Company;
import com.google.gson.Gson;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.UUID;

@Path("/v1/company")
public class CompanyResource {
    @Autowired
    private CompanyDAO companyDAO;

    @GET
    @Produces("application/json")
    public Response getAll(@HeaderParam("api_key") String apiKey){
        try {
            if(companyDAO.findByApiKey(apiKey) == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            Gson gson = new Gson();
            return  Response.ok(gson.toJson(companyDAO.findAll()), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/id/{id}")
    public Response getById(@HeaderParam("api_key") String apiKey, @PathParam("id") String id){
        try {
            if(companyDAO.findByApiKey(apiKey) == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            Gson gson = new Gson();
            if((id == null) || (id.length() < 1)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: id parameter is missing").toJson()).build();
            }
            Company company = companyDAO.findById(Integer.valueOf(id));
            if(company == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(404, "ERROR_DATA_NOT_FOUND", "Error: company not found").toJson()).build();
            }
            return  Response.ok(gson.toJson(companyDAO.findAll()), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @PUT
    @Produces("application/json")
    public Response save(@HeaderParam("api_key") String apiKey, String content){
        try {
            /*if(companyDAO.findByApiKey(apiKey) == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }*/

            Gson gson = new Gson();
            Document dCompany = Document.parse(content);

            Response r = validateParams(dCompany);
            if(r != null){
                return r;
            }

            Company company = new Company();
            company.setNit(dCompany.getString("nit"));
            company.setName(dCompany.getString("name"));
            company.setAlterName(dCompany.getString("alterName"));
            company.setCreateAt(new Timestamp(DateUtils.getActualDate().getTime()));
            company.setApiKey(Crypt.getHash((UUID.randomUUID()).toString(), "MD5"));
            company.setState(dCompany.getInteger("state"));

            Company savedCompany = companyDAO.save(company);
            return  Response.ok(gson.toJson(savedCompany), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @POST
    @Produces("application/json")
    public Response update(@HeaderParam("api_key") String apiKey, String content){
        try {
            if(companyDAO.findByApiKey(apiKey) == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = new Gson();
            Document dCompany = Document.parse(content);

            if(dCompany.get("id") == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: id parameter is missing").toJson()).build();
            }

            Response r = validateParams(dCompany);
            if(r != null){
                return r;
            }

            Company company = companyDAO.findById(dCompany.getInteger("id"));
            if(company == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(404, "ERROR_DATA_NOT_FOUND", "Error: company not found").toJson()).build();
            }

            company.setName(dCompany.getString("name"));
            company.setAlterName(dCompany.getString("alterName"));
            company.setState(dCompany.getInteger("state"));

            Company savedCompany = companyDAO.update(company);
            return  Response.ok(gson.toJson(savedCompany), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @DELETE
    @Produces("application/json")
    @Path("/delete/{id}")
    public Response delete(@HeaderParam("api_key") String apiKey, @PathParam("id") String id){
        try {
            if(companyDAO.findByApiKey(apiKey) == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            Gson gson = new Gson();
            if(id == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: id parameter is missing").toJson()).build();
            }

            Company company = companyDAO.findById(Integer.valueOf(id));
            if(company == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(404, "ERROR_DATA_NOT_FOUND", "Error: company not found").toJson()).build();
            }

            companyDAO.delete(company);
            return  Response.ok(HttpResponses.getResponse(200, "OK", "The company was deleted.").toJson(), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    private Response validateParams(Document d){
        if(d.get("nit") == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: nit parameter is missing").toJson()).build();
        }

        if(d.get("name") == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: name parameter is missing").toJson()).build();
        }

        if(d.get("alterName") == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: alterName parameter is missing").toJson()).build();
        }

        if(d.get("state") == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: state parameter is missing").toJson()).build();
        }

        return null;
    }

}
