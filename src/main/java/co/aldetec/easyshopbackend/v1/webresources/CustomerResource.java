package co.aldetec.easyshopbackend.v1.webresources;

import co.aldetec.easyshopbackend.v1.misc.Crypt;
import co.aldetec.easyshopbackend.v1.misc.CustomGson;
import co.aldetec.easyshopbackend.v1.misc.DateUtils;
import co.aldetec.easyshopbackend.v1.misc.HttpResponses;
import co.aldetec.easyshopbackend.v1.persistence.dao.CompanyDAO;
import co.aldetec.easyshopbackend.v1.persistence.dao.CustomerDAO;
import co.aldetec.easyshopbackend.v1.persistence.dao.CustomerSessionDAO;
import co.aldetec.easyshopbackend.v1.persistence.entities.*;
import com.google.gson.Gson;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.List;

@Path("/v1/customer")
public class CustomerResource {

    @Autowired
    private CustomerDAO customerDAO;

    @Autowired
    private CompanyDAO companyDAO;

    @Autowired
    private CustomerSessionDAO customerSessionDAO;

    @GET
    @Produces("application/json")
    @Path("/id/{id}")
    public Response getById(@HeaderParam("api_key") String apiKey, @PathParam("id") String id){
        try {
            if(companyDAO.findByApiKey(apiKey) == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();
            Customer customer = customerDAO.findById(Integer.valueOf(id));
            if(customer == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "ERROR_DATA_NOT_FOUND", "Error: customer not found").toJson()).build();
            }
            return  Response.ok(gson.toJson(customer), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/email/{email}")
    public Response getByEmail(@HeaderParam("api_key") String apiKey, @PathParam("email") String email){
        try {
            if(companyDAO.findByApiKey(apiKey) == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();
            Customer customer = customerDAO.findByEmail(email);
            if(customer == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(404, "ERROR_DATA_NOT_FOUND", "Error: customer not found").toJson()).build();
            }
            return  Response.ok(gson.toJson(customer), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/cel-number/{celNumber}")
    public Response getByCelNumber(@HeaderParam("api_key") String apiKey, @PathParam("celNumber") String celNumber){
        try {
            if(companyDAO.findByApiKey(apiKey) == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();
            Customer customer = customerDAO.findByCelNumber(celNumber);
            if(customer == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(404, "ERROR_DATA_NOT_FOUND", "Error: customer not found").toJson()).build();
            }
            return  Response.ok(gson.toJson(customer), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @PUT
    @Produces("application/json")
    public Response save(@HeaderParam("api_key") String apiKey, String content){
        try {

            Company company = companyDAO.findByApiKey(apiKey);
            if( company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();
            Document d = Document.parse(content);

            Response r = validateParams("SAVE", d);

            if(r != null){
                return r;
            }

            Customer customer = new Customer();
            customer.setName(d.getString("name"));
            customer.setLastnames(d.getString("lastnames"));
            customer.setEmail(d.getString("email"));
            customer.setCelNumber(d.getString("celNumber"));
            customer.setPassword(Crypt.getHash(d.getString("password"), "MD5"));
            customer.setCompany(company);

            customer = customerDAO.save(customer);

            CustomerSession customerSession = new CustomerSession();

            customerSession.setCustomer(customer);
            customerSession.setCreateAt(new Timestamp(DateUtils.getActualDate().getTime()));
            customerSession.setLastLogin(new Timestamp(DateUtils.getActualDate().getTime()));
            customerSession = customerSessionDAO.save(customerSession);

            return  Response.ok(gson.toJson(customerSession), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @POST
    @Produces("application/json")
    public Response update(@HeaderParam("api_key") String apiKey, String content){
        try {

            Company company = companyDAO.findByApiKey(apiKey);
            if( company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();
            Document d = Document.parse(content);

            Response r = validateParams("UPDATE", d);

            if(r != null){
                return r;
            }

            Customer customer = customerDAO.findById(d.getInteger("id"));
            if(customer == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(404, "ERROR_DATA_NOT_FOUND", "Error: customer not found.").toJson()).build();
            }

            customer.setName(d.getString("name"));
            customer.setLastnames(d.getString("lastnames"));
            customer.setEmail(d.getString("email"));
            customer.setCelNumber(d.getString("celNumber"));
            if((d.get("password") != null) || (d.getString("password").length() > 0)){
                customer.setPassword(Crypt.getHash(d.getString("password"), "MD5"));
            }
            customer = customerDAO.update(customer);
            return  Response.ok(gson.toJson(customer), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @DELETE
    @Produces("application/json")
    @Path("/{id}")
    public Response deleteCategoryLevel2(@HeaderParam("api_key") String apiKey, @PathParam("id") String id){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            Gson gson = CustomGson.getCustomGson();
            Customer customer = customerDAO.findById(Integer.valueOf(id));
            if(customer == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "ERROR_DATA_NOT_FOUND", "Error: customer not found").toJson()).build();
            }
            customerDAO.delete(customer);
            return  Response.ok(HttpResponses.getResponse(200, "SUCCESS", "Customer deleted").toJson(), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @PUT
    @Produces("application/json")
    @Path("/session")
    public Response createSession(@HeaderParam("api_key") String apiKey, String content){
        try {

            Company company = companyDAO.findByApiKey(apiKey);
            if( company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();
            Document d = Document.parse(content);

            if(d.get("customerId") == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: customerId parameter is missing").toJson()).build();
            }

            Customer customer = customerDAO.findById(d.getInteger("customerId"));
            if(customer == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "ERROR_DATA_NOT_FOUND", "Error: customer not found").toJson()).build();
            }

            CustomerSession customerSession = new CustomerSession();
            customerSession.setCustomer(customer);
            customerSession.setCreateAt(new Timestamp(DateUtils.getActualDate().getTime()));
            customerSession.setLastLogin(new Timestamp(DateUtils.getActualDate().getTime()));
            customerSession = customerSessionDAO.save(customerSession);

            return  Response.ok(gson.toJson(customerSession), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/session/{sessionId}")
    public Response getSessionById(@HeaderParam("api_key") String apiKey, @PathParam("sessionId") String sessionId){
        try {

            Company company = companyDAO.findByApiKey(apiKey);
            if( company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();

            if(sessionId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: sessionId parameter is missing").toJson()).build();
            }

            CustomerSession customerSession = customerSessionDAO.findById(Integer.valueOf(sessionId));
            if(customerSession == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "ERROR_DATA_NOT_FOUND", "Error: customerSession not found").toJson()).build();
            }

            return  Response.ok(gson.toJson(customerSession), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @POST
    @Produces("application/json")
    @Path("/login")
    public Response login(@HeaderParam("api_key") String apiKey, String content){
        try {

            Company company = companyDAO.findByApiKey(apiKey);
            if( company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();
            Document d = Document.parse(content);

            if(d.get("email") == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: email parameter is missing").toJson()).build();
            }

            if(d.get("password") == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: email password is missing").toJson()).build();
            }

            Customer customer = customerDAO.findByEmailAndPassword(d.getString("email"), Crypt.getHash(d.getString("password"), "MD5"));
            if(customer == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(404, "ERROR_DATA_NOT_FOUND", "Error: customer not found.").toJson()).build();
            }

            CustomerSession customerSession = new CustomerSession();

            customerSession.setCustomer(customer);
            customerSession.setCreateAt(new Timestamp(DateUtils.getActualDate().getTime()));
            customerSession.setLastLogin(new Timestamp(DateUtils.getActualDate().getTime()));
            customerSession = customerSessionDAO.save(customerSession);

            return  Response.ok(gson.toJson(customerSession), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    private Response validateParams(String action, Document d){
        if((action.equals("UPDATE")) && (d.get("id") == null)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: id parameter is missing").toJson()).build();
        }

        if((action.equals("SAVE") || (action.equals("UPDATE"))) && (d.get("name") == null)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: name parameter is missing").toJson()).build();
        }

        if((action.equals("SAVE") || (action.equals("UPDATE"))) && (d.get("lastnames") == null)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: lastnames parameter is missing").toJson()).build();
        }

        if((action.equals("SAVE") || (action.equals("UPDATE"))) && (d.get("email") == null)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: email parameter is missing").toJson()).build();
        }

        if((action.equals("SAVE") || (action.equals("UPDATE"))) && (d.get("celNumber") == null)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: celNumber parameter is missing").toJson()).build();
        }

        if((action.equals("SAVE")) && (d.get("password") == null)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: name password is missing").toJson()).build();
        }

        return null;
    }
}
