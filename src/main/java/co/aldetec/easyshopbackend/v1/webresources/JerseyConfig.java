package co.aldetec.easyshopbackend.v1.webresources;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig()
    {
        register(CompanyResource.class);
        register(ProductResource.class);
        register(CategoryResource.class);
        register(CustomerResource.class);
        register(OrderResource.class);
        register(CorsFilter.class);

    }

}
