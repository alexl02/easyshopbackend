package co.aldetec.easyshopbackend.v1.webresources;

import co.aldetec.easyshopbackend.v1.mail.Mail;
import co.aldetec.easyshopbackend.v1.misc.Crypt;
import co.aldetec.easyshopbackend.v1.misc.CustomGson;
import co.aldetec.easyshopbackend.v1.misc.DateUtils;
import co.aldetec.easyshopbackend.v1.misc.HttpResponses;
import co.aldetec.easyshopbackend.v1.persistence.dao.*;
import co.aldetec.easyshopbackend.v1.persistence.entities.*;
import com.google.gson.Gson;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static co.aldetec.easyshopbackend.v1.misc.DateUtils.getCurrentDate;

@Path("/v1/order")
public class OrderResource {
    @Autowired
    private CompanyDAO companyDAO;

    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private BranchDAO branchDAO;

    @Autowired
    private CustomerDAO customerDAO;

    @Autowired
    private ProductDAO productDAO;
    @Autowired
    private Mail mail;

    @GET
    @Produces("application/json")
    @Path("/id/{id}")
    public Response getById(@HeaderParam("api_key") String apiKey, @PathParam("id") String id){
        try {
            if(companyDAO.findByApiKey(apiKey) == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            if(id == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: id parameter is missing").toJson()).build();
            }

            Order order = orderDAO.findById(Integer.valueOf(id));
            if(order == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(400, "ERROR_DATA_NOT_FOUND", "Error: order not found").toJson()).build();
            }
            Gson gson = CustomGson.getCustomGson();
            return  Response.ok(gson.toJson(order), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/branch/{branchId}")
    public Response getAllByBranch(@HeaderParam("api_key") String apiKey, @PathParam("branchId") String branchId){
        try {
            if(companyDAO.findByApiKey(apiKey) == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            if(branchId == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: branchId parameter is missing").toJson()).build();
            }

            Branch branch = branchDAO.findById(Integer.valueOf(branchId));
            if(branch == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(400, "ERROR_DATA_NOT_FOUND", "Error: branch not found").toJson()).build();
            }
            Gson gson = CustomGson.getCustomGson();
            return  Response.ok(gson.toJson(orderDAO.findAllByBranch(branch)), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/customer/{customerId}")
    public Response getAllByCustomer(@HeaderParam("api_key") String apiKey, @PathParam("customerId") String customerId){
        try {
            if(companyDAO.findByApiKey(apiKey) == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            if(customerId == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: customerId parameter is missing").toJson()).build();
            }

            Customer customer = customerDAO.findById(Integer.valueOf(customerId));
            if(customer == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(400, "ERROR_DATA_NOT_FOUND", "Error: customer not found").toJson()).build();
            }
            Gson gson = CustomGson.getCustomGson();
            return  Response.ok(gson.toJson(orderDAO.findAllByCustomer(customer)), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @PUT
    @Produces("application/json")
    public Response save(@HeaderParam("api_key") String apiKey, String content){
        try {
            if(companyDAO.findByApiKey(apiKey) == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Document dOrder = Document.parse(content);

            Response r = validateParams("SAVE", dOrder);
            if(r != null){
                return r;
            }

            Branch branch = branchDAO.findById(dOrder.getInteger("branchId"));
            if(branch == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(400, "ERROR_DATA_NOT_FOUND", "Error: branch not found").toJson()).build();
            }

            Customer customer = customerDAO.findById(dOrder.getInteger("customerId"));
            if(customer == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(400, "ERROR_DATA_NOT_FOUND", "Error: customer not found").toJson()).build();
            }

            Order order = new Order();
            order.setAddress(dOrder.getString("address"));
            order.setAddressDetails(dOrder.getString("addressDetails"));
            order.setTelephoneNumber(dOrder.getString("telephoneNumber"));
            order.setZone(dOrder.getString("zone"));
            order.setCreateAt(new Timestamp(getCurrentDate().getTime()));
            order.setBranch(branch);
            order.setCustomer(customer);

            List<Document> dOrderDetails = (List<Document>) dOrder.get("orderDetails");
            if(dOrderDetails.size() < 1) {
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(400, "ERROR_DATA_MISSING", "Error: The order must have 1 product at least.").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();

            List<OrderDetail> orderDetails = new ArrayList<OrderDetail>();
            for( Document dOrderDetail : dOrderDetails ) {
                Product product = productDAO.findById(branch.getCompany(), dOrderDetail.getInteger("productId"));
                if(product == null){
                    continue;
                }

                OrderDetail orderDetail = new OrderDetail();
                orderDetail.setProduct(product);
                orderDetail.setQty(Double.parseDouble(dOrderDetail.get("qty").toString()));
                orderDetail.setMu(dOrderDetail.getString("mu"))
                ;
                orderDetail.setPrice(Double.parseDouble(dOrderDetail.get("price").toString()));
                orderDetail.setDiscountPrice(Double.parseDouble(dOrderDetail.get("discountPrice").toString()));
                orderDetails.add(orderDetail);
            }
            order.setOrderDetails(orderDetails);
            order = orderDAO.save(order);
            mail.sendCustomerMail(order);
            mail.sendOrderMail(order);
            return  Response.ok(gson.toJson(orderDAO.save(order)), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @POST
    @Produces("application/json")
    public Response update(@HeaderParam("api_key") String apiKey, String content){
        try {
            if(companyDAO.findByApiKey(apiKey) == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Document dOrder = Document.parse(content);

            Response r = validateParams("UPDATE", dOrder);
            if(r != null){
                return r;
            }

            Branch branch = branchDAO.findById(dOrder.getInteger("branchId"));
            if(branch == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(400, "ERROR_DATA_NOT_FOUND", "Error: branch not found").toJson()).build();
            }

            Customer customer = customerDAO.findById(dOrder.getInteger("customerId"));
            if(customer == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(400, "ERROR_DATA_NOT_FOUND", "Error: customer not found").toJson()).build();
            }

            Order order = orderDAO.findById(dOrder.getInteger("id"));
            if(customer == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(400, "ERROR_DATA_NOT_FOUND", "Error: order not found").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();

            order.setAddress(dOrder.getString("address"));
            order.setAddressDetails(dOrder.getString("addressDetails"));
            order.setTelephoneNumber(dOrder.getString("telephoneNumber"));
            order.setZone(dOrder.getString("zone"));
            order.setCreateAt(new Timestamp(getCurrentDate().getTime()));
            order.setBranch(branch);
            order.setCustomer(customer);

            return  Response.ok(gson.toJson(orderDAO.save(order)), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    private Response validateParams(String action, Document d){
        if((action.equals("UPDATE")) && (d.get("id") == null)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: id parameter is missing").toJson()).build();
        }

        if((action.equals("SAVE") || (action.equals("UPDATE"))) && (d.get("address") == null)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: address parameter is missing").toJson()).build();
        }

        if((action.equals("SAVE") || (action.equals("UPDATE"))) && (d.get("telephoneNumber") == null)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: telephoneZone parameter is missing").toJson()).build();
        }

        if((action.equals("SAVE") || (action.equals("UPDATE"))) && (d.get("zone") == null)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: zone parameter is missing").toJson()).build();
        }

        if((action.equals("SAVE") || (action.equals("UPDATE"))) && (d.get("orderDetails") == null)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: orderDetails parameter is missing").toJson()).build();
        }

        return null;
    }
}
