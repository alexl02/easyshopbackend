package co.aldetec.easyshopbackend.v1.webresources;

import co.aldetec.easyshopbackend.v1.connectors.ProductConnector;
import co.aldetec.easyshopbackend.v1.misc.Crypt;
import co.aldetec.easyshopbackend.v1.misc.CustomGson;
import co.aldetec.easyshopbackend.v1.misc.DateUtils;
import co.aldetec.easyshopbackend.v1.misc.HttpResponses;
import co.aldetec.easyshopbackend.v1.persistence.dao.*;
import co.aldetec.easyshopbackend.v1.persistence.entities.Branch;
import co.aldetec.easyshopbackend.v1.persistence.entities.Company;
import co.aldetec.easyshopbackend.v1.persistence.entities.ErpProduct;
import co.aldetec.easyshopbackend.v1.persistence.entities.Product;
import com.google.gson.Gson;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Path("/v1/product")
public class ProductResource {
    @Autowired
    private CompanyDAO companyDAO;

    @Autowired
    private ErpProductDAO erpProductDAO;

    @Autowired
    private ErpProductPriceDAO erpProductPriceDAO;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private BranchDAO branchDAO;

    @Autowired
    private ProductConnector productConnector;

    @GET
    @Produces("application/json")
    public Response getAll(@HeaderParam("api_key") String apiKey){
        try {
            if(companyDAO.findByApiKey(apiKey) == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            Gson gson = new Gson();
            return  Response.ok(gson.toJson(erpProductDAO.findAll()), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/categories")
    public Response getByCategories(@HeaderParam("api_key") String apiKey){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            Gson gson = new Gson();
            return  Response.ok(productDAO.findAllByCategory(company), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/mu/all/{id}")
    public Response getAllById(@HeaderParam("api_key") String apiKey, @PathParam("id") String id){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            if(id == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: id parameter is missing").toJson()).build();
            }
            Gson gson = new Gson();
            return  Response.ok(gson.toJson(productDAO.findAllById(company, Integer.valueOf(id))), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/id/{id}")
    public Response getById(@HeaderParam("api_key") String apiKey, @PathParam("id") String id){
        try {
            if(companyDAO.findByApiKey(apiKey) == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            Gson gson = new Gson();
            if((id == null) || (id.length() < 1)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: id parameter is missing").toJson()).build();
            }
            ErpProduct erpProduct = erpProductDAO.findById(Integer.valueOf(id));
            if(erpProduct == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(404, "ERROR_DATA_NOT_FOUND", "Error: product not found").toJson()).build();
            }
            return  Response.ok(gson.toJson(companyDAO.findAll()), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/app/id/{id}/{mu}")
    public Response getProductById(@HeaderParam("api_key") String apiKey, @PathParam("id") String id, @PathParam("mu") String mu){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            Gson gson = new Gson();
            if((id == null) || (id.length() < 1)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: id parameter is missing").toJson()).build();
            }
            Document dProduct = productDAO.findByIdAndMu(company, Integer.valueOf(id), mu);
            if(dProduct == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(404, "ERROR_DATA_NOT_FOUND", "Error: product not found").toJson()).build();
            }
            return  Response.ok(dProduct.toJson(), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/app/category/{categoryId}")
    public Response getProductsByCategory(@HeaderParam("api_key") String apiKey, @PathParam("categoryId") String categoryId){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            Gson gson = new Gson();
            if((categoryId == null) || (categoryId.length() < 1)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: categoryId parameter is missing").toJson()).build();
            }
            Document dCategory = productDAO.findByCategory(company, Integer.valueOf(categoryId));
            if(dCategory == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(404, "ERROR_DATA_NOT_FOUND", "Error: category not found").toJson()).build();
            }
            return  Response.ok(dCategory.toJson(), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/app/subcategory/{subCategoryId}")
    public Response getProductsBySubCategory(@HeaderParam("api_key") String apiKey, @PathParam("subCategoryId") String subCategoryId){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            Gson gson = new Gson();
            if((subCategoryId == null) || (subCategoryId.length() < 1)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: categoryId parameter is missing").toJson()).build();
            }
            Document dSubCategory = productDAO.findBySubCategory(company, Integer.valueOf(subCategoryId));
            if(dSubCategory == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(404, "ERROR_DATA_NOT_FOUND", "Error: category not found").toJson()).build();
            }
            return  Response.ok(dSubCategory.toJson(), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/app/search/{word}")
    public Response getProductsBySearch(@HeaderParam("api_key") String apiKey, @PathParam("word") String word){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            Gson gson = CustomGson.getCustomGson();
            if((word == null) || (word.length() < 1)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: word parameter is missing").toJson()).build();
            }

            word = word.toLowerCase();
            word.replace(" ", "%");

            List<Document> dProducts = productDAO.search(company, word);
            if(dProducts == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(404, "ERROR_DATA_NOT_FOUND", "Error: category not found").toJson()).build();
            }
            return  Response.ok(gson.toJson(dProducts), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/app/search/autocomplete/{word}")
    public Response getProductsForAutocomplete(@HeaderParam("api_key") String apiKey, @PathParam("word") String word){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            Gson gson = CustomGson.getCustomGson();
            if((word == null) || (word.length() < 1)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: word parameter is missing").toJson()).build();
            }

            if(word.length() < 3) {
                return  Response.ok("[]", MediaType.APPLICATION_JSON).build();
            }

            word = word.toLowerCase();
            word.replace(" ", "%");

            List<Document> dProducts = productDAO.searchAutocomplete(company, word);
            if(dProducts == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(404, "ERROR_DATA_NOT_FOUND", "Error: category not found").toJson()).build();
            }
            return  Response.ok(gson.toJson(dProducts), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    /*@GET
    @Produces("application/json")
    @Path("/sku/{sku}")
    public Response getBySku(@HeaderParam("api_key") String apiKey, @PathParam("sku") String sku){
        try {
            if(companyDAO.findByApiKey(apiKey) == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            Gson gson = new Gson();
            if((sku == null) || (sku.length() < 1)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: id parameter is missing").toJson()).build();
            }
            ErpProduct erpProduct = erpProductDAO.findBySku(sku);
            if(erpProduct == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(404, "ERROR_DATA_NOT_FOUND", "Error: product not found").toJson()).build();
            }
            return  Response.ok(gson.toJson(companyDAO.findAll()), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }*/

    @GET
    @Produces("application/json")
    @Path("/ean/{ean}")
    public Response getByEAN(@HeaderParam("api_key") String apiKey, @PathParam("ean") String ean){
        try {
            if(companyDAO.findByApiKey(apiKey) == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }
            Gson gson = new Gson();
            if((ean == null) || (ean.length() < 1)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: id parameter is missing").toJson()).build();
            }
            ErpProduct erpProduct = erpProductDAO.findByEAN(ean);
            if(erpProduct == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(404, "ERROR_DATA_NOT_FOUND", "Error: product not found").toJson()).build();
            }
            return  Response.ok(gson.toJson(companyDAO.findAll()), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @POST
    @Produces("application/json")
    @Path("/many")
    public Response updateMany(@HeaderParam("api_key") String apiKey, String content){
        try {

            Company company = companyDAO.findByApiKey(apiKey);
            if( company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = new Gson();
            Document d = Document.parse(content);

            if(d.get("products") == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: products parameter is missing").toJson()).build();
            }

            if(d.get("erpBranchId") == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: erpBranchId parameter is missing").toJson()).build();
            }

            Branch branch = branchDAO.findByCompanyAndErpBranchId(company, d.getString("erpBranchId"));

            erpProductDAO.updateMany(branch, (List<Document>) d.get("products"));

            return  Response.ok(HttpResponses.getResponse(200, "OK", "Success.").toJson(), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @POST
    @Produces("application/json")
    @Path("/app/many")
    public Response updateAppMany(@HeaderParam("api_key") String apiKey, String content){
        try {

            Company company = companyDAO.findByApiKey(apiKey);
            if( company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = new Gson();
            Document d = Document.parse(content);

            if(d.get("products") == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: products parameter is missing").toJson()).build();
            }

            if(d.get("erpBranchId") == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: erpBranchId parameter is missing").toJson()).build();
            }

            Branch branch = branchDAO.findByCompanyAndErpBranchId(company, d.getString("erpBranchId"));

            productDAO.updateManyByBranch(branch, (List<Document>) d.get("products"));

            return  Response.ok(HttpResponses.getResponse(200, "OK", "Success.").toJson(), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @POST
    @Produces("application/json")
    @Path("/prices/many")
    public Response updatePricesMany(@HeaderParam("api_key") String apiKey, String content){
        try {

            Company company = companyDAO.findByApiKey(apiKey);
            if( company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = new Gson();
            Document d = Document.parse(content);

            if(d.get("prices") == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: prices parameter is missing").toJson()).build();
            }

            if(d.get("erpBranchId") == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: erpBranchId parameter is missing").toJson()).build();
            }

            Branch branch = branchDAO.findByCompanyAndErpBranchId(company, d.getString("erpBranchId"));
            if(branch == null){
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(404, "ERROR_DATA_NOT_FOUND", "Error: branch not found").toJson()).build();
            }

            erpProductPriceDAO.updateMany(branch, (List<Document>) d.get("prices"));

            return  Response.ok(HttpResponses.getResponse(200, "OK", "Success.").toJson(), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/connector")
    public Response loadByConnector(@HeaderParam("api_key") String apiKey){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            productConnector.loadConnector(company);
            return Response.status(Response.Status.OK).entity(HttpResponses.getResponse(200, "SUCCESS", "categories loaded succesfully").toJson()).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }
}
