package co.aldetec.easyshopbackend.v1.webresources;

import co.aldetec.easyshopbackend.v1.misc.CustomGson;
import co.aldetec.easyshopbackend.v1.misc.HttpResponses;
import co.aldetec.easyshopbackend.v1.persistence.dao.CompanyDAO;
import co.aldetec.easyshopbackend.v1.persistence.dao.ShoppingCartDAO;
import co.aldetec.easyshopbackend.v1.persistence.entities.Company;
import co.aldetec.easyshopbackend.v1.persistence.entities.ShoppingCart;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/v1/shopping-cart")
public class ShoppingCartResource {

    @Autowired
    ShoppingCartDAO shoppingCartDAO;

    @Autowired
    CompanyDAO companyDAO;

    @GET
    @Produces("application/json")
    @Path("/session/{sessionId}")
    public Response getBySessionId(@HeaderParam("api_key") String apiKey, @PathParam("sessionId") String sessionId){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            if( sessionId == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(HttpResponses.getResponse(400, "ERROR_PARAMETER_MISSING", "Error: sessionId parameter is missing").toJson()).build();
            }
            Gson gson = CustomGson.getCustomGson();
            return  Response.ok(gson.toJson(shoppingCartDAO.findBySessionId(sessionId)), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }

    @PUT
    @Produces("application/json")
    public Response save(@HeaderParam("api_key") String apiKey, String content){
        try {
            Company company = companyDAO.findByApiKey(apiKey);
            if(company == null){
                return Response.status(Response.Status.FORBIDDEN).entity(HttpResponses.getResponse(403, "FORBIDDEN", "Invalid apiKey").toJson()).build();
            }

            Gson gson = CustomGson.getCustomGson();
            ShoppingCart shoppingCart = gson.fromJson(content, ShoppingCart.class);

            return  Response.ok(gson.toJson(shoppingCartDAO.save(shoppingCart)), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(HttpResponses.getResponse(500, "INTERNAL_ERROR", "Error: " + ex.toString()).toJson()).build();
        }
    }
}
