INSERT INTO public.company (id, alter_name, apikey, create_at, name, nit, state) VALUES (1, 'Pruebas S.A.S.', 'cec3c6a996891875a89756a19680a7a5', '2022-07-02 12:40:39.238', 'PRUEBAS S.A.S.', '999999999', 1);

INSERT INTO public.branch (id, erp_branch_id, name, state, company_id) VALUES (1, '002', 'TRUCK PLAYA', true, 1);
INSERT INTO public.branch (id, erp_branch_id, name, state, company_id) VALUES (2, '003', 'TRUCK INFINITY', true, 1);
INSERT INTO public.branch (id, erp_branch_id, name, state, company_id) VALUES (3, '004', 'TRUCK SPACE', true, 1);
